from __future__ import print_function

from keras.models import Input, Model
from keras.layers import Dense, SeparableConv2D, Conv2D, Concatenate, MaxPooling2D, Add
from keras.layers import UpSampling2D, Dropout, BatchNormalization
from keras.constraints import non_neg

class SpectralDimReduce(object):
    """A simple multi-channel autoencoder in Keras"""

    def __init__(self):
        super(SpectralDimReduce, self).__init__()
        self._activation = 'relu'
        self._dropout = True
        self_batchNorm = False

        self.dataShape = None
        self.input = None  # Holder for Model input
        self.output = None  # Holder for Model output

    def begin(self, input_shape, output_shape):
        """
        Input here is an image of the forward scatter intensity
        of the measured data (c_0=1); In imaging terms, that's a 1-channel 2D image.
        """
        self.input_shape = input_shape
        self.output_shape = output_shape
        print("input shape {}".format(self.input_shape))
        self.input = Input(shape=self.input_shape)

    def finalize(self):
        """
        The expected output data is the scatter prediction; in train-test scenarios,
        this is the Monte Carlo simulation of the scattering.
        The output variable holds the network to achieve that.
        The function returns the model itself.
        """
        if self.input is None:
            raise RuntimeError("Missing an input. Use begin().")
        if self.output is None:
            raise RuntimeError("Missing an output. Use buildNetwork().")
        return Model(inputs=self.input, outputs=self.output)

    def buildNetwork(self):
        """
        This function builds the network exactly as-is from the paper of Maier et al.
        """
        #=== Downsizing Convolutional ===#
        #print("Created input layer with shape {}".format(self.input.shape))
        #numChannels = self.dataShape[2]
        ## Encoder
        #conv1_1_1 = Conv2D(int(numChannels / 2), (3, 3), activation=self._activation, padding='same', input_shape=inShapePerImage, data_format="channels_last")(self.input)
        #pool1 = MaxPooling2D((2, 2), padding='same')(conv1_1_1)
        ##pool1 = Dense(int(numChannels / 4), activation=self._activation)(conv1_1_2)
        #print("Created conv layer 1 with shape {}".format(pool1.shape))
        #conv1_2_1 = Conv2D(int(numChannels / 8), (3, 3), activation=self._activation, padding='same')(pool1)
        #pool2 = MaxPooling2D((2, 2), padding='same')(conv1_2_1)
        ##pool2 = Dense(int(numChannels / 16), activation=self._activation)(conv1_2_2)
        #print("Created conv layer 2 with shape {}".format(pool2.shape))
        #conv1_3_1 = Conv2D(int(numChannels / 16), (3, 3), activation=self._activation, padding='same')(pool2)
        #pool3 = MaxPooling2D((2, 2), padding='same')(conv1_3_1)
        ##pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        #print("Created conv layer 3 with shape {}".format(pool3.shape))
        #convcenter = Conv2D(1, (3, 3), activation=self._activation, padding='same')(pool3)
        ## Decoder
        #conv2_1_1 = Conv2D(int(numChannels / 16), (3, 3), activation=self._activation, padding='same')(convcenter)
        #up1 = UpSampling2D((2, 2))(conv2_1_1)
        ##up1 = Dense(int(numChannels / 16), activation=self._activation)(conv2_1_2)
        #print("Created deconv layer 3 with shape {}".format(up1.shape))
        #conv2_2_1 = Conv2D(int(numChannels / 8), (3, 3), activation=self._activation, padding='same')(up1)
        #up2 = UpSampling2D((2, 2))(conv2_2_1)
        ##up2 = Dense(int(numChannels / 4), activation=self._activation)(conv2_2_2)
        #print("Created deconv layer 2 with shape {}".format(up2.shape))
        #conv2_3_1 = Conv2D(int(numChannels / 2), (3, 3), activation=self._activation, padding='same')(up2)
        #up3 = UpSampling2D((2, 2))(conv2_3_1)
        ##up3 = Dense(int(numChannels / 2), activation=self._activation)(conv2_3_2)
        #print("Created deconv layer 1 with shape {}".format(up3.shape))
        #self.output = Conv2D(numChannels, (3, 3), activation='sigmoid', padding='same', data_format="channels_last")(up3)
        #print("Created output layer with shape {}".format(self.output.shape))


        print("Created input layer with shape {}".format(self.input.shape))
        numChannels = self.input_shape[2]
        # Encoder
        #conv1_1_1 = Conv2D(int(numChannels * 2), (3, 3), activation=self._activation, padding='same', input_shape=inShapePerImage, data_format="channels_last")(self.input)
        conv1_1_1 = SeparableConv2D(int(numChannels * 1.5), (3, 3), depth_multiplier=2, activation=self._activation,
                                    padding='same', input_shape=self.input_shape, data_format="channels_last")(self.input)
        bn1 = BatchNormalization()(conv1_1_1)
        do1 = Dropout(0.3)(bn1)
        # pool1 = Dense(int(numChannels / 4), activation=self._activation)(conv1_1_2)
        print("Created conv_1 layer 1 (feature increase) with shape {}".format(do1.shape))
        conv1_2_1 = Conv2D(int(numChannels * 1.75), (3, 3), activation=self._activation, padding='same')(do1)
        bn2 = BatchNormalization()(conv1_2_1)
        # pool2 = Dense(int(numChannels / 16), activation=self._activation)(conv1_2_2)
        print("Created conv_2 layer 1 (feature increase) with shape {}".format(bn2.shape))
        conv1_3_1 = Conv2D(int(numChannels * 2), (3, 3), activation=self._activation, padding='same')(bn2)
        bn3 = BatchNormalization()(conv1_3_1)
        # pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        print("Created conv_3 layer 1 (feature increase) with shape {}".format(bn3.shape))

        # pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)

        conv1_4_1 = Conv2D(int(numChannels * 2), (3, 3), activation=self._activation, padding='same')(bn3)
        bn4 = BatchNormalization()(conv1_4_1)
        do4 = Dropout(0.3)(bn4)
        print("Created conv_1 layer 2 (feature learning) with shape {}".format(do4.shape))
        conv1_5_1 = Conv2D(int(numChannels * 2), (3, 3), activation=self._activation, padding='same')(do4)
        bn5 = BatchNormalization()(conv1_5_1)
        do5 = Dropout(0.3)(bn5)
        print("Created conv_2 layer 2 (feature learning) with shape {}".format(do5.shape))
        conv1_6_1 = Conv2D(int(numChannels * 2), (3, 3), activation=self._activation, padding='same')(do5)
        bn6 = BatchNormalization()(conv1_6_1)
        do6 = Dropout(0.3)(bn6)
        print("Created conv_3 layer 2 (feature learning) with shape {}".format(do6.shape))

        midConv = Conv2D(int(numChannels * 2), (3, 3), activation=self._activation, padding='same')(do6)
        print("Created mid_conv layer 2 (feature learning) with shape {}".format(midConv.shape))
        midDense = Dense(int(numChannels), activation=self._activation)(midConv)
        print("Created mid_dense layer 2 (feature learning) with shape {}".format(midDense.shape))

        #=== Reduce Dimensionality ===#
        #= could also be done by Pooling the feature layers OR by Conv3D(1, (3,3,3) ) and then reduce the 3rd dim via pooling =#
        sconv_1 = Conv2D(int(numChannels), (3, 3), activation=self._activation, padding='same')(midDense)
        bn_s1 = BatchNormalization()(sconv_1)
        print("Created conv_1 layer 3 (channel reduction) with shape {}".format(bn_s1.shape))
        sconv_2 = Conv2D(int(numChannels / 2), (3, 3), activation=self._activation, padding='same')(bn_s1)
        bn_s2 = BatchNormalization()(sconv_2)
        print("Created conv_2 layer 3 (channel reduction) with shape {}".format(bn_s2.shape))
        sconv_3 = Conv2D(int(numChannels / 4), (3, 3), activation=self._activation, padding='same')(bn_s2)
        bn_s3 = BatchNormalization()(sconv_3)
        print("Created conv_3 layer 3 (channel reduction) with shape {}".format(bn_s3.shape))
        sconv_4 = Conv2D(int(numChannels / 8), (3, 3), activation=self._activation, padding='same')(bn_s3)
        bn_s4 = BatchNormalization()(sconv_4)
        print("Created conv_4 layer 3 (channel reduction) with shape {}".format(bn_s4.shape))
        sconv_5 = Conv2D(int(numChannels / 16), (3, 3), activation=self._activation, padding='same')(bn_s4)
        bn_s5 = BatchNormalization()(sconv_5)
        print("Created conv_5 layer 3 (channel reduction) with shape {}".format(bn_s5.shape))
        sconv_6 = Conv2D(int(numChannels / 32), (3, 3), activation=self._activation, padding='same')(bn_s5)
        bn_s6 = BatchNormalization()(sconv_6)
        print("Created conv_6 layer 3 (channel reduction) with shape {}".format(bn_s6.shape))
        #, output_shape=self.output_shape
        self.output = SeparableConv2D(self.output_shape[2], (3, 3), depth_multiplier=1, kernel_constraint=non_neg(), activation=self._activation, padding='same', data_format="channels_last")(bn_s6)
        print("Created output layer with shape {}".format(self.output.shape))