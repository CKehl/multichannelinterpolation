from __future__ import print_function

from keras.models import Input, Model
from keras.layers import Dense, SeparableConv2D, Conv2D, Concatenate, MaxPooling2D, Add
from keras.layers import UpSampling2D, Dropout
from keras.constraints import non_neg

class SpectralDimReduce(object):
    """A simple multi-channel autoencoder in Keras"""

    def __init__(self):
        super(SpectralDimReduce, self).__init__()
        self._activation = 'relu'
        self._dropout = True
        self._kernel_size = 3

        self.dataShape = None
        self.input = None  # Holder for Model input
        self.output = None  # Holder for Model output

    def begin(self, input_shape, output_shape):
        """
        Input here is an image of the forward scatter intensity
        of the measured data (c_0=1); In imaging terms, that's a 1-channel 2D image.
        """
        self.input_shape = input_shape
        self.output_shape = output_shape
        print("input shape {}".format(self.input_shape))
        self.input = Input(shape=self.input_shape)

    def finalize(self):
        """
        The expected output data is the scatter prediction; in train-test scenarios,
        this is the Monte Carlo simulation of the scattering.
        The output variable holds the network to achieve that.
        The function returns the model itself.
        """
        if self.input is None:
            raise RuntimeError("Missing an input. Use begin().")
        if self.output is None:
            raise RuntimeError("Missing an output. Use buildNetwork().")
        return Model(inputs=self.input, outputs=self.output)

    def buildNetwork(self):
        """
        This function builds the network exactly as-is from the paper of Maier et al.
        """
        print("Created input layer with shape {}".format(self.input.shape))
        numChannels = self.input_shape[2]
        # Encoder
        #conv1_1_1 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same',
        # input_shape=self.input_shape, data_format="channels_last")(self.input)
        # WORKING FOR XRAY
        #input_conv = SeparableConv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), depth_multiplier=2, activation=self._activation,
        #                            padding='same', input_shape=self.input_shape, data_format="channels_last")(self.input)
        # === Comment: remember that the scattering can be negative too! === #
        input_conv = SeparableConv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), depth_multiplier=2,
                                     activation=self._activation, padding='same', input_shape=self.input_shape,
                                     data_format="channels_last")(self.input)
        dropout1_l1 = Dropout(0.2)(input_conv)
        print("Created conv_1 layer 1 (feature increase) with shape {}".format(dropout1_l1.shape))

        conv1_l2 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout1_l1)
        dropout1_l2 = Dropout(0.2)(conv1_l2)
        print("Created conv_1 layer 2 (feature learning) with shape {}".format(dropout1_l2.shape))
        conv2_l2 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout1_l2)
        dropout2_l2 = Dropout(0.2)(conv2_l2)
        print("Created conv_2 layer 2 (feature learning) with shape {}".format(dropout2_l2.shape))
        #conv3_l2 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout2_l2)
        #dropout3_l2 = Dropout(0.2)(conv3_l2)
        #print("Created conv_3 layer 2 (feature learning) with shape {}".format(dropout3_l2.shape))

        midConv = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout2_l2)
        midDropout = Dropout(0.2)(midConv)
        print("Created mid_conv layer 2 (feature learning) with shape {}".format(midConv.shape))
        mconv_1 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation,padding='same')(midDropout)
        dropout1_mid = Dropout(0.2)(mconv_1)
        dense_1 = Dense(int(numChannels * 2), activation=self._activation)(dropout1_mid)
        mconv_2 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation,padding='same')(dense_1)
        dropout2_mid = Dropout(0.2)(mconv_2)
        dense_2 = Dense(int(numChannels * 2), activation=self._activation)(dropout2_mid)
        # ACTIVE ONLY FOR SCATTERING
        #mconv_3 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation,padding='same')(dense_2)
        #dropout3_mid = Dropout(0.2)(mconv_3)
        #dense_3 = Dense(int(numChannels * 2), activation=self._activation)(dropout3_mid)

        #midDense = Dense(int(numChannels), activation=self._activation)(midConv)
        midDense = Dense(int(numChannels), activation=self._activation)(dense_2)
        #midDense = Dense(int(numChannels), activation=self._activation)(mconv_3)
        print("Created mid_dense layer 2 (feature learning) with shape {}".format(midDense.shape))

        #=== Reduce Dimensionality ===#
        #= could also be done by Pooling the feature layers OR by Conv3D(1, (3,3,3) ) and then reduce the 3rd dim via pooling =#
        conv1_l3 = Conv2D(int(numChannels), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(midDense)
        print("Created conv_1 layer 3 (channel reduction) with shape {}".format(conv1_l3.shape))
        #dense1_l3 = Dense(int(numChannels / 1), activation=self._activation)(midDense)
        #print("Created conv_1 layer 3 (channel reduction) with shape {}".format(dense1_l3.shape))
        conv2_l3 = Conv2D(int(numChannels / 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv1_l3)
        print("Created conv_2 layer 3 (channel reduction) with shape {}".format(conv2_l3.shape))
        #dense2_l3 = Dense(int(numChannels / 2), activation=self._activation)(dense1_l3)
        #print("Created conv_2 layer 3 (channel reduction) with shape {}".format(dense2_l3.shape))
        conv3_l3 = Conv2D(int(numChannels / 4), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv2_l3)
        print("Created conv_3 layer 3 (channel reduction) with shape {}".format(conv3_l3.shape))
        #dense3_l3 = Dense(int(numChannels / 4), activation=self._activation)(dense2_l3)
        #print("Created conv_3 layer 3 (channel reduction) with shape {}".format(dense3_l3.shape))
        conv4_l3 = Conv2D(int(numChannels / 8), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv3_l3)
        print("Created conv_4 layer 3 (channel reduction) with shape {}".format(conv4_l3.shape))
        #dense4_l3 = Dense(int(numChannels / 8), activation=self._activation)(dense3_l3)
        #print("Created conv_4 layer 3 (channel reduction) with shape {}".format(dense4_l3.shape))
        conv5_l3 = Conv2D(int(numChannels / 16), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv4_l3)
        print("Created conv_5 layer 3 (channel reduction) with shape {}".format(conv5_l3.shape))
        #dense5_l3 = Dense(int(numChannels / 16), activation=self._activation)(dense4_l3)
        #print("Created conv_5 layer 3 (channel reduction) with shape {}".format(dense5_l3.shape))
        conv6_l3 = Conv2D(int(numChannels / 32), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv5_l3)
        print("Created conv_6 layer 3 (channel reduction) with shape {}".format(conv6_l3.shape))
        #dense6_l3 = Dense(int(numChannels / 32), activation=self._activation)(dense5_l3)
        #print("Created conv_6 layer 3 (channel reduction) with shape {}".format(dense6_l3.shape))
        #self.output = SeparableConv2D(self.output_shape[2], (1, 1), depth_multiplier=1, kernel_constraint=non_neg(), activation=self._activation, padding='same', data_format="channels_last")(dense6_l3)
        #self.output = SeparableConv2D(self.output_shape[2], (1, 1), depth_multiplier=1, kernel_constraint=non_neg(), activation=self._activation, padding='same', data_format="channels_last")(conv6_l3)
        # === Comment: Do not use 1x1 convolutions - somehow that makes the model learn nothing and the loss stays at ~0.5 all the time === #
        #self.output = SeparableConv2D(self.output_shape[2], (self._kernel_size, self._kernel_size), depth_multiplier=1, kernel_constraint=non_neg(),activation=self._activation, padding='same', data_format="channels_last")(conv6_l3)
        # === Comment: remember that the scattering can be negative too! === #
        self.output = SeparableConv2D(self.output_shape[2], (self._kernel_size, self._kernel_size), depth_multiplier=1,
                                      activation=self._activation, padding='same', data_format="channels_last")(conv6_l3)
        print("Created output layer with shape {}".format(self.output.shape))