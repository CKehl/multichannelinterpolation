from __future__ import print_function

from keras.models import Input, Model
from keras.layers import Dense, SeparableConv2D, Conv2D, Concatenate, MaxPooling2D, Add
from keras.layers import UpSampling2D, Dropout, BatchNormalization
from keras.constraints import non_neg

class SimpleAutoencoder(object):
    """A simple multi-channel autoencoder in Keras"""

    def __init__(self):
        super(SimpleAutoencoder, self).__init__()
        self._activation = 'relu'
        self._dropout = True

        self.dataShape = None
        self.input = None  # Holder for Model input
        self.output = None  # Holder for Model output

    def begin(self, image_shape):
        """
        Input here is an image of the forward scatter intensity
        of the measured data (c_0=1); In imaging terms, that's a 1-channel 2D image.
        """
        self.dataShape = image_shape
        print("input shape {}".format(image_shape))
        self.input = Input(shape=image_shape)

    def finalize(self):
        """
        The expected output data is the scatter prediction; in train-test scenarios,
        this is the Monte Carlo simulation of the scattering.
        The output variable holds the network to achieve that.
        The function returns the model itself.
        """
        if self.input is None:
            raise RuntimeError("Missing an input. Use begin().")
        if self.output is None:
            raise RuntimeError("Missing an output. Use buildNetwork().")
        return Model(inputs=self.input, outputs=self.output)

    def buildNetwork(self, inShapePerImage=None):
        """
        This function builds the network exactly as-is from the paper of Maier et al.
        """
        #=== Downsizing Convolutional ===#
        #print("Created input layer with shape {}".format(self.input.shape))
        #numChannels = self.dataShape[2]
        ## Encoder
        #conv1_1_1 = Conv2D(int(numChannels / 2), (3, 3), activation=self._activation, padding='same', input_shape=inShapePerImage, data_format="channels_last")(self.input)
        #pool1 = MaxPooling2D((2, 2), padding='same')(conv1_1_1)
        ##pool1 = Dense(int(numChannels / 4), activation=self._activation)(conv1_1_2)
        #print("Created conv layer 1 with shape {}".format(pool1.shape))
        #conv1_2_1 = Conv2D(int(numChannels / 8), (3, 3), activation=self._activation, padding='same')(pool1)
        #pool2 = MaxPooling2D((2, 2), padding='same')(conv1_2_1)
        ##pool2 = Dense(int(numChannels / 16), activation=self._activation)(conv1_2_2)
        #print("Created conv layer 2 with shape {}".format(pool2.shape))
        #conv1_3_1 = Conv2D(int(numChannels / 16), (3, 3), activation=self._activation, padding='same')(pool2)
        #pool3 = MaxPooling2D((2, 2), padding='same')(conv1_3_1)
        ##pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        #print("Created conv layer 3 with shape {}".format(pool3.shape))
        #convcenter = Conv2D(1, (3, 3), activation=self._activation, padding='same')(pool3)
        ## Decoder
        #conv2_1_1 = Conv2D(int(numChannels / 16), (3, 3), activation=self._activation, padding='same')(convcenter)
        #up1 = UpSampling2D((2, 2))(conv2_1_1)
        ##up1 = Dense(int(numChannels / 16), activation=self._activation)(conv2_1_2)
        #print("Created deconv layer 3 with shape {}".format(up1.shape))
        #conv2_2_1 = Conv2D(int(numChannels / 8), (3, 3), activation=self._activation, padding='same')(up1)
        #up2 = UpSampling2D((2, 2))(conv2_2_1)
        ##up2 = Dense(int(numChannels / 4), activation=self._activation)(conv2_2_2)
        #print("Created deconv layer 2 with shape {}".format(up2.shape))
        #conv2_3_1 = Conv2D(int(numChannels / 2), (3, 3), activation=self._activation, padding='same')(up2)
        #up3 = UpSampling2D((2, 2))(conv2_3_1)
        ##up3 = Dense(int(numChannels / 2), activation=self._activation)(conv2_3_2)
        #print("Created deconv layer 1 with shape {}".format(up3.shape))
        #self.output = Conv2D(numChannels, (3, 3), activation='sigmoid', padding='same', data_format="channels_last")(up3)
        #print("Created output layer with shape {}".format(self.output.shape))


        print("Created input layer with shape {}".format(self.input.shape))
        numChannels = self.dataShape[2]
        # Encoder
        #conv1_1_1 = Conv2D(int(numChannels * 2), (3, 3), activation=self._activation, padding='same', input_shape=inShapePerImage, data_format="channels_last")(self.input)
        conv1_1_1 = SeparableConv2D(int(numChannels * 4), (3, 3), depth_multiplier=2, activation=self._activation,
                                    padding='same', input_shape=inShapePerImage, data_format="channels_last")(self.input)
        bn1 = BatchNormalization()(conv1_1_1)
        do1 = Dropout(0.3)(bn1)
        pool1 = MaxPooling2D((2, 2), padding='same')(do1) #256x256 -> 128x128
        # pool1 = Dense(int(numChannels / 4), activation=self._activation)(conv1_1_2)
        print("Created conv layer 1 with shape {}".format(pool1.shape))
        conv1_2_1 = Conv2D(int(numChannels * 3), (3, 3), activation=self._activation, padding='same')(pool1)
        #conv1_2_1 = SeparableConv2D(int(numChannels * 3), (3, 3), activation=self._activation, padding='same')(pool1)
        bn2 = BatchNormalization()(conv1_2_1)
        do2 = Dropout(0.3)(bn2)
        pool2 = MaxPooling2D((2, 2), padding='same')(do2) #128x128 -> 64x64
        # pool2 = Dense(int(numChannels / 16), activation=self._activation)(conv1_2_2)
        print("Created conv layer 2 with shape {}".format(pool2.shape))
        conv1_3_1 = Conv2D(int(numChannels * 6), (3, 3), activation=self._activation, padding='same')(pool2)
        #conv1_3_1 = SeparableConv2D(int(numChannels * 6), (3, 3), activation=self._activation, padding='same')(pool2)
        bn3 = BatchNormalization()(conv1_3_1)
        do3 = Dropout(0.3)(bn3)
        pool3 = MaxPooling2D((2, 2), padding='same')(do3) #64x64 -> 32x32
        # pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        print("Created conv layer 3 with shape {}".format(pool3.shape))

        conv1_4_1 = Conv2D(int(numChannels * 12), (3, 3), activation=self._activation, padding='same')(pool3)
        #conv1_4_1 = SeparableConv2D(int(numChannels * 9), (3, 3), activation=self._activation, padding='same')(pool3)
        bn4 = BatchNormalization()(conv1_4_1)
        do4 = Dropout(0.3)(bn4)
        pool4 = MaxPooling2D((2, 2), padding='same')(do4) #32x32 -> 16x16
        # pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        print("Created conv layer 4 with shape {}".format(pool4.shape))
        conv1_5_1 = Conv2D(int(numChannels * 18), (3, 3), activation=self._activation, padding='same')(pool4)
        #conv1_5_1 = SeparableConv2D(int(numChannels * 18), (3, 3), activation=self._activation, padding='same')(pool4)
        bn5 = BatchNormalization()(conv1_5_1)
        do5 = Dropout(0.3)(bn5)
        pool5 = MaxPooling2D((2, 2), padding='same')(do5) #16x16 -> 8x8
        # pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        print("Created conv layer 5 with shape {}".format(pool5.shape))
        conv1_6_1 = Conv2D(int(numChannels * 36), (3, 3), activation=self._activation, padding='same')(pool5)
        #conv1_5_1 = SeparableConv2D(int(numChannels * 18), (3, 3), activation=self._activation, padding='same')(pool4)
        bn6 = BatchNormalization()(conv1_6_1)
        do6 = Dropout(0.3)(bn6)
        pool6 = MaxPooling2D((2, 2), padding='same')(do6) #8x8 -> 4x4
        # pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        print("Created conv layer 6 with shape {}".format(pool6.shape))

        #convcenter = SeparableConv2D(int(numChannels * 18), (3, 3), activation=self._activation, padding='same')(pool5)
        convcenter = Conv2D(int(numChannels * 54), (3, 3), activation=self._activation, padding='same')(pool6)
        print("Created middle conv layer with shape {}".format(convcenter.shape))
        #convcenter = Conv2D(int(numChannels * 32), (3, 3), activation=self._activation, padding='same')(pool3)

        # Decoder
        # up1 = Dense(int(numChannels / 16), activation=self._activation)(conv2_1_2)
        up0 = UpSampling2D((2, 2))(convcenter)
        conv2_0_1 = Conv2D(int(numChannels * 36), (3, 3), activation=self._activation, padding='same')(up0)
        #conv2_0_1 = SeparableConv2D(int(numChannels * 18), (3, 3), activation=self._activation, padding='same')(up0)
        print("Created deconv layer 6 with shape {}".format(conv2_0_1.shape))
        bnu0 = BatchNormalization()(conv2_0_1)
        # up1 = Dense(int(numChannels / 16), activation=self._activation)(conv2_1_2)
        up1 = UpSampling2D((2, 2))(bnu0)
        conv2_1_1 = Conv2D(int(numChannels * 18), (3, 3), activation=self._activation, padding='same')(up1)
        #conv2_1_1 = SeparableConv2D(int(numChannels * 18), (3, 3), activation=self._activation, padding='same')(up1)
        print("Created deconv layer 5 with shape {}".format(conv2_1_1.shape))
        bnu1 = BatchNormalization()(conv2_1_1)
        # up1 = Dense(int(numChannels / 16), activation=self._activation)(conv2_1_2)
        up2 = UpSampling2D((2, 2))(bnu1)
        conv2_2_1 = Conv2D(int(numChannels * 12), (3, 3), activation=self._activation, padding='same')(up2)
        #conv2_2_1 = SeparableConv2D(int(numChannels * 9), (3, 3), activation=self._activation, padding='same')(up2)
        print("Created deconv layer 4 with shape {}".format(conv2_2_1.shape))
        bnu2 = BatchNormalization()(conv2_2_1)

        # up1 = Dense(int(numChannels / 16), activation=self._activation)(conv2_1_2)
        up3 = UpSampling2D((2, 2))(bnu2)
        #up3 = UpSampling2D((2, 2))(convcenter)
        conv2_3_1 = Conv2D(int(numChannels * 6), (3, 3), activation=self._activation, padding='same')(up3)
        #conv2_3_1 = SeparableConv2D(int(numChannels * 6), (3, 3), activation=self._activation, padding='same')(up3)
        print("Created deconv layer 3 with shape {}".format(conv2_3_1.shape))
        bnu3 = BatchNormalization()(conv2_3_1)
        # up2 = Dense(int(numChannels / 4), activation=self._activation)(conv2_2_2)
        up4 = UpSampling2D((2, 2))(bnu3)
        conv2_4_1 = Conv2D(int(numChannels * 3), (3, 3), activation=self._activation, padding='same')(up4)
        #conv2_4_1 = SeparableConv2D(int(numChannels * 3), (3, 3), activation=self._activation, padding='same')(up4)
        print("Created deconv layer 2 with shape {}".format(conv2_4_1.shape))
        bnu4 = BatchNormalization()(conv2_4_1)
        # up3 = Dense(int(numChannels / 2), activation=self._activation)(conv2_3_2)
        up5 = UpSampling2D((2, 2))(bnu4)
        conv2_5_1 = Conv2D(int(numChannels * 2), (3, 3), activation=self._activation, padding='same')(up5)
        #conv2_5_1 = SeparableConv2D(int(numChannels * 4), (3, 3), depth_multiplier=2, activation=self._activation, padding='same')(up5)
        print("Created deconv layer 1 with shape {}".format(conv2_5_1.shape))
        bnu5 = BatchNormalization()(conv2_5_1)
        #self.output = SeparableConv2D(numChannels, (3, 3), depth_multiplier=1, kernel_constraint=non_neg(), activation='sigmoid', padding='same', data_format="channels_last")(up5)
        self.output = SeparableConv2D(numChannels, (3, 3), depth_multiplier=1, kernel_constraint=non_neg(), activation=self._activation, padding='same', data_format="channels_last")(bnu5)
        print("Created output layer with shape {}".format(self.output.shape))