#!python3
"""
Created on Sept 17 2018

@author: chke (Christian Kehl)
"""
import pickle
import numpy
from scipy import misc
import matplotlib.pyplot as plt
from keras.utils import plot_model
from keras.models import load_model
import tensorflow as tf
import itertools
from skimage import util
from skimage import transform

from argparse import ArgumentParser
import sys
import os
import time
from scipy import io
import h5py
import string
import glob
import re

NORM_DATA_MODE = 0  # 0 - per image over all channels; 1 - per image per channel
TYPES = {"XRAY": 0, "SCATTER": 1, "CT": 2}

def numpy_normalize(v):
    norm = numpy.linalg.norm(v)
    if norm == 0:
        return v
    return v/norm

def normaliseFieldArray(a, numChannels, flatField=None, itype=TYPES["XRAY"]):
    minx = None
    maxx = None
    if NORM_DATA_MODE == 0:
        minx = numpy.min(a)
        maxx = numpy.max(a)
        a = (a - minx) / (maxx - minx)
    elif NORM_DATA_MODE == 1:
        # numChannels = inImage.shape[2]
        minx = []
        maxx = []
        for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
            minx.append(numpy.min(a[:, :, channelIdx]))
            maxx.append(numpy.max(a[:, :, channelIdx]))
            a[:, :, channelIdx] = (a[:, :, channelIdx] - minx[channelIdx]) / (
                    maxx[channelIdx] - minx[channelIdx])
    elif NORM_DATA_MODE == 2:
        if itype == TYPES['XRAY']:
            a = numpy.clip(numpy.divide(a, flatField), 0.0, 1.0)
        elif itype == TYPES['SCATTER']:
            if numChannels<=1:
                #minx = numpy.min(flatField)
                #maxx = numpy.max(flatField)
                minx = 0
                maxx = numpy.max(numpy.abs(a))
                a = (a-minx)/(maxx-minx)
            else:
                minx = []
                maxx = []
                for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                    #minx.append(numpy.min(flatField[:, :, channelIdx]))
                    #maxx.append(numpy.max(flatField[:, :, channelIdx]))
                    minx.append(0)
                    maxx.append(numpy.max(numpy.abs(a[:, :, channelIdx])))
                    a[:, :, channelIdx] = (a[:, :, channelIdx] - minx[channelIdx]) / (maxx[channelIdx] - minx[channelIdx])
        elif self.x_type == TYPES['CT']:
            if numChannels<=1:
                minx = numpy.min(a)
                maxx = numpy.max(a)
                a = (a - minx) / (maxx-minx)
            else:
                minx = []
                maxx = []
                for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                    minx.append(numpy.min(a[:, :, channelIdx]))
                    maxx.append(numpy.max(a[:, :, channelIdx]))
                    a[:, :, channelIdx] = (a[:, :, channelIdx] - minx[channelIdx]) / (maxx[channelIdx] - minx[channelIdx])
    return minx, maxx, a

def denormaliseFieldArray(a, numChannels, minx=None, maxx=None, flatField=None, itype=TYPES["XRAY"]):
    if NORM_DATA_MODE == 0:
        a = a * (maxx - minx) + minx
    elif NORM_DATA_MODE == 1:
        for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
            a[:, :, channelIdx] = a[:, :, channelIdx] * (maxx[channelIdx] - minx[channelIdx]) + \
                                  minx[channelIdx]
    elif NORM_DATA_MODE == 2:
        if itype == TYPES['XRAY']:
            a = a * flatField
        elif itype == TYPES['SCATTER']:
            if numChannels <= 1:
                a = a*(maxx-minx)+minx
            else:
                for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                    a[:, :, channelIdx] = a[:, :, channelIdx] * (maxx[channelIdx] - minx[channelIdx]) + minx[channelIdx]
        elif itype == TYPES['CT']:
            if numChannels <= 1:
                a = a * (maxx - minx) + minx
            else:
                for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                    a[:, :, channelIdx] = a[:, :, channelIdx] * (maxx[channelIdx] - minx[channelIdx]) + minx[channelIdx]
    return a

if __name__ == '__main__':
    '''
    Show models and images and error rate 
    
    Variables:
        
    '''
    artifact=0 # show artifacts
    optionParser = ArgumentParser(description="visualisation routine for CT denoise DeepLearning")
    optionParser.add_argument("-m","--modelpath",action="store",dest="modelpath",default="",help="path to model parent folder (i.e. parent of output- and model folder)")
    optionParser.add_argument("-D","--dataPath-in",action="store",dest="dataPath_in",help="full path to the input images (validation projections)")
    optionParser.add_argument("-d", "--dataPath-out", action="store", dest="dataPath_out",
                              help="full path to the reference output images (e.g. single-energy validation projections)")
    optionParser.add_argument("-O","--outputPath",action="store",dest="outputPath",help="path to store output images")
    optionParser.add_argument("-M","--modelname",action="store",nargs='*',dest="modelname",help="name of the model file(s); if 1 given, just normal vis; if multiple given, then comparison.")
    #optionParser.add_argument("-H","--hist_fname",action="store",dest="hist_fname",help="full path with filename to specific history file")
    optionParser.add_argument("-F","--flatField", action="store", dest="flatField", help="full path to flat field file")
    #options = optionParser.parse_args(args=sys.argv)
    options = optionParser.parse_args()
    argDict = vars(options)
    
    outPath = ""
    modelName = ["../data/models/unet_imagesinv_strid1_relu_v1_5_64_2_model.h5",] # Model file
    weightsName = ["../data/models/unet_imagesinv_strid1_relu_v1_5_64_2_weights.h5",] # weights file
    Mhistfile = ["../data/output/unet_imagesinv_strid1_relu_v1_5_64_2_Thist.pkl",] # Optimization Error History file
    histdir = ""
    if("modelpath" in argDict) and (argDict["modelpath"]!=None):
        if("modelname" in argDict) and (argDict["modelname"]!=None):
            modelName=[]
            weightsName=[]
            Mhistfile = []
            head = argDict["modelpath"]
            for entry in argDict["modelname"]:
                modelName.append(os.path.join(head, "models", entry+"_model.h5"))
                weightsName.append(os.path.join(head, "models", entry+"_weights.h5"))
                Mhistfile.append(os.path.join(head, 'output', entry+"_Thist.pkl"))
                histdir = os.path.join(head, 'output')
            outPath = os.path.join(head, os.path.pardir, "output")
    print(modelName)
    print(weightsName)
    
    #histdir = ""
    #if(argDict["hist_fname"]!=None):
    #    Mhistfile=[]
    #    Mhistfile.append(argDict["hist_fname"])
    #    histdir = os.path.dirname(argDict["hist_fname"])

    if(argDict["outputPath"]!=None):
        outPath = argDict["outputPath"]

    flatField=None
    flatField_in=None
    flatField_out=None
    if("flatField" in argDict) and (argDict["flatField"]!=None):
        f = h5py.File(argDict["flatField"], 'r')
        darray = numpy.array(f['data']['value']) #f['data0']
        f.close()
        flatField = darray
        NORM_DATA_MODE = 2

    #=============================================================================#
    #=== General setup: what is the input/output datatype, what is SE/MECT ... ===#
    #=============================================================================#
    input_type = TYPES["SCATTER"]
    output_type = TYPES["SCATTER"]

    #==========================================================================#
    #====   S T A R T   O F   C O L L E C T I N G   F I L E   P A T H S    ====#
    #==========================================================================#
    inputFileArray = []
    outputFileArray = []
    if argDict["dataPath_in"]==None:
        exit()
    else:
        if input_type == TYPES["XRAY"]:
            for name in glob.glob(os.path.join(argDict["dataPath_in"],'*X*.h5')):
                inputFileArray.append(name)
        elif input_type == TYPES["SCATTER"]:
            for name in glob.glob(os.path.join(argDict["dataPath_in"],'*Y*.h5')):
                inputFileArray.append(name)

    if argDict["dataPath_out"]==None:
        exit()
    else:
        if input_type == TYPES["XRAY"]:
            for name in glob.glob(os.path.join(argDict["dataPath_out"],'*X*.h5')):
                outputFileArray.append(name)
        elif input_type == TYPES["SCATTER"]:
            for name in glob.glob(os.path.join(argDict["dataPath_out"],'*Y*.h5')):
                outputFileArray.append(name)

    itype = None
    dumpDataFile = h5py.File(inputFileArray[0], 'r')
    dumpData_in = numpy.array(dumpDataFile['data']['value'])
    dumpDataFile.close()
    if len(dumpData_in.shape)>3:
        channelNum_in = dumpData_in.shape[3]
    else:
        channelNum_in = dumpData_in.shape[2]
    itype = dumpData_in.dtype
    sqShape_in = numpy.squeeze(dumpData_in).shape

    otype = None
    dumpDataFile = h5py.File(outputFileArray[0], 'r')
    dumpData_out = numpy.array(dumpDataFile['data']['value'])
    dumpDataFile.close()
    if(len(dumpData_out.shape)<3):
        dumpData_out = numpy.reshape(dumpData_out, dumpData_out.shape+(1,))
    if len(dumpData_out.shape)>3:
        channelNum_out = dumpData_out.shape[3]
    else:
        channelNum_out = dumpData_out.shape[2]
    print("output shape: {}".format(dumpData_out.shape))
    otype=dumpData_out.dtype
    sqShape_out = numpy.squeeze(dumpData_out).shape

    if ("flatField" in argDict) and (argDict["flatField"] != None):
        #adapt flat field size
        flatField_in = transform.resize(flatField, (256,256,32), order=3, mode='reflect')
        flatField_out = numpy.array(flatField_in)
        if channelNum_out == 1:
            flatField_out = numpy.squeeze(numpy.sum(flatField_out,2))

    # sort the names
    digits = re.compile(r'(\d+)')
    def tokenize(filename):
        return tuple(int(token) if match else token for token, match in ((fragment, digits.search(fragment)) for fragment in digits.split(filename)))
    # Now you can sort your file names like so:
    inputFileArray.sort(key=tokenize)
    outputFileArray.sort(key=tokenize)
    #==========================================================================#
    #====     E N D   O F   C O L L E C T I N G   F I L E   P A T H S     =====#
    #==========================================================================#
    
    Mhist=pickle.load(open( Mhistfile[0], "rb" ) )
    # Comment CK: the Mhist file represents (as it seems) the 'metrics' field of a
    # Keras model (see: https://keras.io/models/model/ and https://keras.io/metrics/).
    #print(Mhist.keys())
    # summarize history for error function
    plt.figure("erf",figsize=(6, 6), dpi=300)
    #plt.plot(Mhist['mean_absolute_error'])
    #plt.plot(Mhist['val_mean_absolute_error'])
    plt.plot(Mhist['mean_squared_error'])
    plt.plot(Mhist['val_mean_squared_error'])
    #plt.title('mean absolute error')
    plt.title('mean squared error')
    plt.ylabel('erf(x)')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(os.path.join(outPath,"meanSqrErr.png"), dpi=300, format="png")
    plt.close("all")
    #mseArray = numpy.array([Mhist['mean_absolute_error'], Mhist['val_mean_absolute_error']])
    mseArray = numpy.array([Mhist['mean_squared_error'], Mhist['val_mean_squared_error']])
    #mseFile = h5py.File(os.path.join(outPath,"mae.h5"),'w')
    mseFile = h5py.File(os.path.join(outPath, "mse.h5"), 'w')
    mseFile.create_dataset('data', data=mseArray.transpose());
    mseFile.close() 
    #plt.show()
    # summarize history for loss
    plt.figure("loss",figsize=(6, 6), dpi=300)
    plt.plot(Mhist['loss'])
    plt.plot(Mhist['val_loss'])
    #plt.plot(Mhist['val_mean_absolute_error'])
    plt.title('loss function')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(os.path.join(outPath,"loss.png"), dpi=300, format="png")
    plt.close("all")
    maeArray = numpy.array([Mhist['loss'], Mhist['val_loss']])
    maeFile = h5py.File(os.path.join(outPath,"loss_mse.h5"),'w')
    maeFile.create_dataset('data', data=maeArray.transpose());
    maeFile.close()
    del mseArray
    del maeArray
    #plt.show()
    #print (type(Mhist))
    
    lenX = len(inputFileArray)
    lenY = len(outputFileArray)

    for chidx in range(0, channelNum_in):
        chpath = os.path.join(outPath, "channel%03d" % (chidx))
        if os.path.exists(chpath) == False:
            os.mkdir(chpath)

    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    with sess.as_default():
        model = load_model(modelName[0])
        model.load_weights(weightsName[0])
        
        plot_model(model, to_file='model.png', show_shapes=True)
        
        modelComp = None
        if len(modelName)>1:
            modelComp = load_model(modelName[0])

        #imgArr = numpy.zeros((min(lenX,50),5,256,256,32), dtype=otype)
        errArr = numpy.zeros((min(lenX,50),256,256), dtype=otype)
        for imagenr in itertools.islice(itertools.count(), 0, min(lenX,50)):

            file = h5py.File(inputFileArray[imagenr],'r')
            # adaptation for reading the non-normalized data
            inImage = numpy.array(file['data']['value'])
            file.close()
            minValX = None
            maxValX = None
            inImage = transform.resize(inImage, (256,256,channelNum_in), order=3, mode='reflect')
            minValX, maxValX, inImage = normaliseFieldArray(inImage, channelNum_in, flatField_in,input_type)
            if len(inImage.shape) < 3:
                inImage = inImage.reshape((1,) + inImage.shape + (1,))
            elif len(inImage.shape) < 4:
                inImage = inImage.reshape((1,) + inImage.shape)
            inImage = inImage.astype(numpy.float32)

            file = h5py.File(outputFileArray[imagenr],'r')
            outImage = numpy.array(file['data']['value'])
            file.close()
            minValY = None
            maxValY = None
            if channelNum_out>1:
                outImage = transform.resize(outImage, (256,256,channelNum_out), order=3, mode='reflect')
            else:
                outImage = transform.resize(outImage, (256,256), order=3, mode='reflect')
            minValY, maxValY, outImage = normaliseFieldArray(outImage, channelNum_out, flatField_out,output_type)
            if len(outImage.shape) < 3:
                outImage = outImage.reshape((1,) + outImage.shape + (1,))
            elif len(outImage.shape) < 4:
                outImage = outImage.reshape((1,) + outImage.shape)
            outImage = outImage.astype(numpy.float32)
            #print("Range: {} to {} (delta = {})".format(minValY,maxValY,maxValY-minValY))

            # here: in-time prediction - could be stored / precalculated ... ?
            start_predict = time.time()
            predictImage = model.predict(inImage)
            end_predict = time.time()
            print("model prediction took %f seconds" % (end_predict-start_predict))

            #print("Input type: {}".format(itype))
            #print("Output type: {}".format(otype))

            inImage = inImage.astype(itype).squeeze()
            inImage = denormaliseFieldArray(inImage,channelNum_in,minValX,maxValX,flatField_in,input_type)
            #print(inImage[0])
            #print(inImage.dtype)

            predictImage = predictImage.astype(otype).squeeze()
            predictImage = denormaliseFieldArray(predictImage,channelNum_out,minValY,maxValY,flatField_out,output_type)
            #print(predictImage)
            #print(predictImage.dtype)

            outImage = outImage.astype(otype).squeeze()
            outImage = denormaliseFieldArray(outImage,channelNum_out,minValY,maxValY,flatField_out,output_type)
            #print(outImage)
            #print(outImage.dtype)

            #====================================================================#
            #==== Error estimation - SSD metric between input and prediction ====#
            #====================================================================#
            errArr[imagenr,:,:] = outImage-predictImage
            imgErr = errArr[imagenr,:,:]
            normErr = numpy_normalize(imgErr).astype(numpy.float32)

            errArr[imagenr,:,:] = numpy.square(errArr[imagenr,:,:])
            imgErr = numpy.square(imgErr)
            imgError = numpy.mean(imgErr)
            normErr = numpy.square(normErr)
            normError = numpy.mean(normErr)
            print("MSE img %d: %g" % (imagenr, imgError))
            print("normalized MSE img %d: %g" % (imagenr, normError))
            # ==================================================================== #
            # == End error estimation - SSD metric between input and prediction == #
            # ==================================================================== #

            # ============== #
            # == Plotting == #
            # ============== #
            for channelIdx in itertools.islice(itertools.count(), 0, channelNum_in):
                imgName = "predict%04d.png" % imagenr
                imgPath = os.path.join(outPath, "channel%03d" % (channelIdx), imgName)
                plt.figure(imagenr, figsize=(8, 5), dpi=300)

                plt.subplot(131)
                plt.imshow(inImage[:, :, channelIdx].squeeze(), cmap='gray')
                plt.title("Projection.")
                plt.subplot(132)
                if channelNum_out ==channelNum_in:
                    plt.imshow(predictImage[:, :, channelIdx].squeeze(), cmap='gray')
                    plt.title("Prediction")
                    plt.subplot(133)
                    plt.imshow(outImage[:, :, channelIdx].squeeze(), cmap='gray')
                    plt.title("Groundtruth")
                elif channelNum_out>1:
                    plt.imshow(predictImage[:, :, 0].squeeze(), cmap='gray')
                    plt.title("Prediction")
                    plt.subplot(133)
                    plt.imshow(outImage[:, :, 0].squeeze(), cmap='gray')
                    plt.title("Groundtruth")
                else:
                    plt.imshow(predictImage[:, :].squeeze(), cmap='gray')
                    plt.title("Prediction")
                    plt.subplot(133)
                    plt.imshow(outImage[:, :].squeeze(), cmap='gray')
                    plt.title("Groundtruth")
                plt.savefig(imgPath, dpi=300, format="png")
                plt.close("all")


