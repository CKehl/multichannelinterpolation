#!python3
import pickle
import numpy
from scipy import misc
import matplotlib.pyplot as plt
from keras.utils import plot_model
from keras.models import load_model
import tensorflow as tf
import itertools
from skimage import util
from skimage import transform

from argparse import ArgumentParser
import sys
import os
import time
from scipy import io
import h5py
import string
import glob
import re

NORM_DATA_MODE = 0  # 0 - per image over all channels; 1 - per image per channel

def numpy_normalize(v):
    norm = numpy.linalg.norm(v)
    if norm == 0:
        return v
    return v/norm

def normaliseFieldArray(a, numChannels, flatField=None):
    minx = None
    maxx = None
    if NORM_DATA_MODE == 0:
        minx = numpy.min(a)
        maxx = numpy.max(a)
        a = (a - minx) / (maxx - minx)
    elif NORM_DATA_MODE == 1:
        # numChannels = inImage.shape[2]
        minx = []
        maxx = []
        for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
            minx.append(numpy.min(inImage[:, :, channelIdx]))
            maxx.append(numpy.max(inImage[:, :, channelIdx]))
            a[:, :, channelIdx] = (a[:, :, channelIdx] - minValX[channelIdx]) / (
                        maxValX[channelIdx] - minValX[channelIdx])
    elif NORM_DATA_MODE == 2:
        a = numpy.clip(numpy.divide(a, flatField), 0.0, 1.0)
    return minx, maxx, a

def denormaliseFieldArray(a, numChannels, minx=None, maxx=None, flatField=None):
    if NORM_DATA_MODE == 0:
        a = a * (maxx - minx) + minx
    elif NORM_DATA_MODE == 1:
        for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
            a[:, :, channelIdx] = a[:, :, channelIdx] * (maxx[channelIdx] - minx[channelIdx]) + \
                                  minx[channelIdx]
    elif NORM_DATA_MODE == 2:
        a = a * flatField
    return a

if __name__ == '__main__':
    '''
    Show models and images and error rate 
    
    Variables:
        
    '''
    artifact=0 # show artifacts
    optionParser = ArgumentParser(description="visualisation routine for CT denoise DeepLearning")
    optionParser.add_argument("-m","--modelpath",action="store",dest="modelpath",default="",help="path to model parent folder (i.e. parent of output- and model folder)")
    optionParser.add_argument("-D","--dataPath",action="store",dest="dataPath",help="full path to the input images (validation projections)")
    optionParser.add_argument("-O","--outputPath",action="store",dest="outputPath",help="path to store output images")
    optionParser.add_argument("-M","--modelname",action="store",nargs='*',dest="modelname",help="name of the model file(s); if 1 given, just normal vis; if multiple given, then comparison.")
    optionParser.add_argument("-H","--hist_fname",action="store",dest="hist_fname",help="full path with filename to specific history file")
    optionParser.add_argument("-F", "--flatField", action="store", dest="flatField", help="full path to flat field file")
    #options = optionParser.parse_args(args=sys.argv)
    options = optionParser.parse_args()

    argDict = vars(options)
    
    outPath = ""
    modelName = ["../data/models/unet_imagesinv_strid1_relu_v1_5_64_2_model.h5",] # Model file
    weightsName = ["../data/models/unet_imagesinv_strid1_relu_v1_5_64_2_weights.h5",] # weights file
    Mhistfile = ["../data/output/unet_imagesinv_strid1_relu_v1_5_64_2_Thist.pkl",] # Optimization Error History file
    if("modelpath" in argDict) and (argDict["modelpath"]!=None):
        if("modelname" in argDict) and (argDict["modelname"]!=None):
            modelName=[]
            weightsName=[]
            head = argDict["modelpath"]
            for entry in argDict["modelname"]:
                modelName.append(os.path.join(head, "models", entry+"_model.h5"))
                weightsName.append(os.path.join(head, "models", entry+"_weights.h5"))
            outPath = os.path.join(head, os.path.pardir, "output")
    print(modelName)
    print(weightsName)
    
    histdir = ""
    if(argDict["hist_fname"]!=None):
        Mhistfile=[]
        Mhistfile.append(argDict["hist_fname"])
        histdir = os.path.dirname(argDict["hist_fname"])

    if(argDict["outputPath"]!=None):
        outPath = argDict["outputPath"]

    flatField=None
    if("flatField" in argDict) and (argDict["flatField"]!=None):
        f = h5py.File(argDict["flatField"], 'r')
        darray = numpy.array(f['data']['value']) #f['data0']
        f.close()
        flatField = darray
        NORM_DATA_MODE = 2

    #==========================================================================#
    #====   S T A R T   O F   C O L L E C T I N G   F I L E   P A T H S    ====#
    #==========================================================================#
    inputFileArray = []
    observationFileArray = []
    scatterFileArray = []
    if argDict["dataPath"]==None:
        exit()
    else:
        for name in glob.glob(os.path.join(argDict["dataPath"],'*X*.h5')):
            inputFileArray.append(name)
        #for name in glob.glob(os.path.join(argDict["dataPath"],'*Y*.h5')):
        #    scatterFileArray.append(name)
        #for name in glob.glob(os.path.join(argDict["dataPath"],'*Z*.h5')):
        #    observationFileArray.append(name)

    otype = None
    dumpDataFile = h5py.File(inputFileArray[0], 'r')
    dumpData = numpy.array(dumpDataFile['data']['value'])
    dumpDataFile.close()
    channelNum = numpy.squeeze(dumpData).shape[2]
    otype = dumpData.dtype
    sqShape = numpy.squeeze(dumpData).shape
    if ("flatField" in argDict) and (argDict["flatField"] != None):
        #adapt flat field size
        flatField = transform.resize(flatField, (256,256,32), order=3, mode='reflect')
    # sort the names
    digits = re.compile(r'(\d+)')
    def tokenize(filename):
        return tuple(int(token) if match else token for token, match in ((fragment, digits.search(fragment)) for fragment in digits.split(filename)))
    # Now you can sort your file names like so:
    inputFileArray.sort(key=tokenize)
    #scatterFileArray.sort(key=tokenize)
    #observationFileArray.sort(key=tokenize)
    #==========================================================================#
    #====     E N D   O F   C O L L E C T I N G   F I L E   P A T H S     =====#
    #==========================================================================#
    
    Mhist=pickle.load(open( Mhistfile[0], "rb" ) )
    # Comment CK: the Mhist file represents (as it seems) the 'metrics' field of a
    # Keras model (see: https://keras.io/models/model/ and https://keras.io/metrics/).
    #print(Mhist.keys())
    # summarize history for error function
    plt.figure("erf",figsize=(6, 6), dpi=300)
    plt.plot(Mhist['mean_absolute_error'])
    plt.plot(Mhist['val_mean_absolute_error'])
    #plt.plot(Mhist['val_loss'])
    plt.title('mean absolute error')
    plt.ylabel('erf(x)')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(os.path.join(outPath,"meanAbsErr.png"), dpi=300, format="png")
    plt.close("all")
    mseArray = numpy.array([Mhist['mean_absolute_error'], Mhist['val_mean_absolute_error']])
    mseFile = h5py.File(os.path.join(outPath,"mae.h5"),'w')
    mseFile.create_dataset('data', data=mseArray.transpose());
    mseFile.close() 
    #plt.show()
    # summarize history for loss
    plt.figure("loss",figsize=(6, 6), dpi=300)
    plt.plot(Mhist['loss'])
    plt.plot(Mhist['val_loss'])
    #plt.plot(Mhist['val_mean_absolute_error'])
    plt.title('loss function')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(os.path.join(outPath,"loss.png"), dpi=300, format="png")
    plt.close("all")
    maeArray = numpy.array([Mhist['loss'], Mhist['val_loss']])
    maeFile = h5py.File(os.path.join(outPath,"loss_mse.h5"),'w')
    maeFile.create_dataset('data', data=maeArray.transpose());
    maeFile.close()
    del mseArray
    del maeArray
    #plt.show()
    #print (type(Mhist))
    
    lenX = len(inputFileArray)
    #lenY = len(observationFileArray)

    for chidx in range(0, channelNum):
        chpath = os.path.join(outPath, "channel%03d" % (chidx))
        if os.path.exists(chpath) == False:
            os.mkdir(chpath)

    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    with sess.as_default():
        model = load_model(modelName[0])
        model.load_weights(weightsName[0])
        
        plot_model(model, to_file='model.png', show_shapes=True)
        
        modelComp = None
        if len(modelName)>1:
            modelComp = load_model(modelName[0])

        #imgArr = numpy.zeros((min(lenX,50),5,256,256,32), dtype=otype)
        errArr = numpy.zeros((min(lenX,50),256,256,32), dtype=otype)
        for imagenr in itertools.islice(itertools.count(), 0, min(lenX,50)):

            file = h5py.File(inputFileArray[imagenr],'r')
            #inImage = numpy.array(file['data']['value'], dtype=numpy.float32)
            #file.close()
            # adaptation for reading the non-normalized data
            inImage = numpy.array(file['data']['value'])
            file.close()
            minValX = None
            maxValX = None
            numChannels = inImage.shape[2]
            inImage = transform.resize(inImage, (256,256,inImage.shape[2]), order=3, mode='reflect')

            #if NORM_DATA_MODE == 0:
            #    minValX = numpy.min(inImage)
            #    maxValX = numpy.max(inImage)
            #    inImage = (inImage - minValX)/(maxValX-minValX)
            #elif NORM_DATA_MODE == 1:
            #    #numChannels = inImage.shape[2]
            #    minValX = []
            #    maxValX = []
            #    for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
            #        minValX.append(numpy.min(inImage[:,:,channelIdx]))
            #        maxValX.append(numpy.max(inImage[:,:,channelIdx]))
            #        inImage[:,:,channelIdx] = (inImage[:,:,channelIdx] - minValX[channelIdx]) / (maxValX[channelIdx] - minValX[channelIdx])
            #elif NORM_DATA_MODE == 2:
            #    inImage = numpy.clip(numpy.divide(inImage,flatField), 0.0, 1.0)
            minValX, maxValX, inImage = normaliseFieldArray(inImage, numChannels, flatField)

            inImage = inImage.astype(numpy.float32)
            if len(inImage.shape) < 3:
                inImage = inImage.reshape((1,) + inImage.shape + (1,))
            elif len(inImage.shape) < 4:
                inImage = inImage.reshape((1,) + inImage.shape)


            # here: in-time prediction - could be stored / precalculated ... ?
            start_predict = time.time()
            predictImage = model.predict(inImage.reshape(inImage.shape+(1,)))
            end_predict = time.time()
            print("model prediction took %f seconds" % (end_predict-start_predict))

            inImage = inImage.astype(otype).squeeze()
            #if NORM_DATA_MODE == 0:
            #    inImage = inImage*(maxValX-minValX)+minValX
            #elif NORM_DATA_MODE == 1:
            #    for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
            #        inImage[:,:,channelIdx] = inImage[:,:,channelIdx]*(maxValX[channelIdx]-minValX[channelIdx])+minValX[channelIdx]
            #elif NORM_DATA_MODE == 2:
            #    inImage = inImage*flatField
            inImage = denormaliseFieldArray(inImage,numChannels,minValX,maxValX,flatField)

            predictImage = predictImage.astype(otype).squeeze()
            predictImage = denormaliseFieldArray(predictImage,numChannels,minValX,maxValX,flatField)

            #====================================================================#
            #==== Error estimation - SSD metric between input and prediction ====#
            #====================================================================#
            errArr[imagenr,:,:,:] = inImage-predictImage
            imgErr = errArr[imagenr,:,:,:]
            normErr = None
            if NORM_DATA_MODE == 0:
                normErr = numpy_normalize(imgErr).astype(numpy.float32)
            else:
                normErr = numpy.zeros((imgErr.shape[0],imgErr.shape[1],imgErr.shape[2]), dtype=numpy.float32)
                for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                    normErr[:,:,channelIdx] = numpy_normalize(imgErr[:,:,channelIdx]).astype(numpy.float32)
            errArr[imagenr,:,:,:] = numpy.square(errArr[imagenr,:,:,:])
            imgErr = numpy.square(imgErr)
            imgError = numpy.mean(imgErr)
            normErr = numpy.square(normErr)
            normError = numpy.mean(normErr)
            print("MSE img %d: %g" % (imagenr, imgError))
            print("normalized MSE img %d: %g" % (imagenr, normError))
            # ==================================================================== #
            # == End error estimation - SSD metric between input and prediction == #
            # ==================================================================== #

            # ============== #
            # == Plotting == #
            # ============== #
            for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                imgName = "predict%04d.png" % imagenr
                imgPath = os.path.join(outPath, "channel%03d" % (channelIdx), imgName)
                plt.figure(imagenr, figsize=(8, 5), dpi=300)

                plt.subplot(151)
                plt.imshow(inImage[:, :, channelIdx].squeeze(), cmap='gray')
                plt.title("Projection.")
                plt.subplot(152)
                plt.imshow(predictImage[:, :, channelIdx].squeeze(), cmap='gray')
                plt.title("Prediction")
                plt.subplot(153)
                plt.imshow(inImage[:, :, channelIdx].squeeze(), cmap='gray')
                plt.title("Groundtruth")
                plt.savefig(imgPath, dpi=300, format="png")
                plt.close("all")


