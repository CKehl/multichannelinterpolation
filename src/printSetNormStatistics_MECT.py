#!/usr/bin/python
'''
Created on Sept 22, 2018

@author: christian
'''
import numpy
import h5py
import glob
import re
import os
#from optparse import OptionParser
from argparse import ArgumentParser
import itertools

if __name__ == '__main__':
    useFlatField = True
    optionParser = ArgumentParser("usage: %prog -i <input hdf5 file> [-n <field name inside mat file>]")
    optionParser.add_argument("-i","--inputDir",action="store",dest="inputDir",default="",help="directory with input data files")
    optionParser.add_argument("-o","--outputDir",action="store",dest="outputDir",default="",help="directory for output files")
    optionParser.add_argument("-I","--InputDirs",action="store",nargs='*',dest="inputDirs",help="multiple directories with input data files")
    #optionParser.add_argument("-O","--OutputDirs",action="store",nargs='*',dest="outputDirs",help="multiple directories for each train, test and validation data")
    optionParser.add_argument("-f", "--fieldName", action="store", dest="fieldName", default="data",
                            help="optional: field name to be extracted from .mat file")
    optionParser.add_argument("-F", "--flatField", action="store", dest="flatFieldPath", help="path to flat field file")
    #(options, args) = optionParser.parse_args()
    options = optionParser.parse_args()
    argDict = vars(options)

    fieldName = options.fieldName

    inputFileArray = []
    scatterFileArray = []
    observationFileArray = []
    if("inputDirs" in argDict) and (argDict["inputDirs"]!=None):
        dirArray = []
        for dirEntry in argDict["inputDirs"]:
            dirArray.append(dirEntry)
        print(dirArray)
        for entry in dirArray:
            for name in glob.glob(os.path.join(entry,'*X*.h5')):
                inputFileArray.append(name)
            for name in glob.glob(os.path.join(entry,'*Y*.h5')):
                scatterFileArray.append(name)
            for name in glob.glob(os.path.join(entry,'*Z*.h5')):
                observationFileArray.append(name)
    else:#if ("inputDir" in argDict) and (argDict["inputDir"]!=None):
        for name in glob.glob(os.path.join(argDict["inputDir"],'*X*.h5')):
            inputFileArray.append(name)
        for name in glob.glob(os.path.join(argDict["inputDir"],'*Y*.h5')):
            scatterFileArray.append(name)
        for name in glob.glob(os.path.join(argDict["inputDir"],'*Z*.h5')):
            observationFileArray.append(name)
    # sort the names
    digits = re.compile(r'(\d+)')
    def tokenize(filename):
        return tuple(int(token) if match else token for token, match in ((fragment, digits.search(fragment)) for fragment in digits.split(filename)))
    # Now you can sort your file names like so:
    inputFileArray.sort(key=tokenize)
    scatterFileArray.sort(key=tokenize)
    observationFileArray.sort(key=tokenize)
    #print(inputFileArray)
    #print(inputFileArray[0])
    #print(scatterFileArray[0])
    #print(observationFileArray[0])


    # ====== Determine data characteristics first (type, dim, order, etc.) ====== #
    dumpDataFile = h5py.File(inputFileArray[0], 'r')
    dumpData = numpy.array(dumpDataFile['data']['value'], order='F').transpose()
    dumpDataFile.close()

    dArray = numpy.squeeze(dumpData)
    numDims = len(dumpData.shape)
    arrayType = dumpData.dtype

    flatField=None
    if (options.flatFieldPath != None) and (len(options.flatFieldPath)>0) and useFlatField:
        f = h5py.File(argDict["flatFieldPath"], "r")
        flatField = numpy.array(f['data']['value'], order='F').transpose()
        f.close()

    outputDir = "."
    if ("outputDir" in argDict) and (argDict["outputDir"]!=None) and (len(argDict["outputDir"])>0):
        outputDir = argDict["outputDir"]
    #meanStatFilePath = os.path.join(outputDir,"meanStat.csv")
    #medianStatFilePath = os.path.join(outputDir,"medianStat.csv")
    #stdDevStatFilePath = os.path.join(outputDir,"stdDevStat.csv")
    #varStatFilePath = os.path.join(outputDir,"varStat.csv")

    # ======================================= #
    # === first: radiographic projections === #
    # ======================================= #
    radiographic_mean_SECT = 0
    radiographic_median_SECT = 0
    radiographic_stdDev_SECT = 0
    radiographic_variance_SECT = 0
    radiographic_mean_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    radiographic_median_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    radiographic_stdDev_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    radiographic_variance_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    meanStatFilePath = os.path.join(outputDir,"meanStat_xray.csv")
    medianStatFilePath = os.path.join(outputDir,"medianStat_xray.csv")
    stdDevStatFilePath = os.path.join(outputDir,"stdDevStat_xray.csv")
    varStatFilePath = os.path.join(outputDir,"varStat_xray.csv")
    #for fEntry in inputFileArray:
    for fEidx in range(0,len(inputFileArray)):
        fEntry = inputFileArray[fEidx]
        dArray = numpy.zeros((1,1,1))
        f = h5py.File(fEntry,'r')
        dArray = numpy.array(f[fieldName]['value'], order='F').transpose()
        f.close()
        dArray = numpy.clip(numpy.divide(dArray, flatField), 0.0, 1.0)
        #for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[2]):
        #    minValX = 0
        #    maxValX = numpy.max(numpy.abs(dArray[:, :, channelIdx]))
        #    dArray[:, :, channelIdx] = (dArray[:, :, channelIdx] - minValX) / (maxValX - minValX)

        if numDims==2:
            mean = numpy.nanmean(dArray, None)
            median = numpy.nanmedian(dArray, None)
            stdDev = numpy.nanstd(dArray, None)
            variance = numpy.nanvar(dArray, None)
            #print("Mean:\t\t\t{}".format(mean))
            #print("Median:\t\t\t{}".format(median))
            #print("std. Deviation:\t{}".format(stdDev))
            #print("variance\t\t{}:".format(variance))
            radiographic_mean_SECT+=mean
            radiographic_median_SECT+=median
            radiographic_stdDev_SECT+=stdDev
            radiographic_variance_SECT+=variance
            with open(meanStatFilePath, "a") as meanStatFile:
                meanStatFile.write("{},{}\n".format(fEidx,radiographic_mean_SECT))
            with open(medianStatFilePath, "a") as medianStatFile:
                medianStatFile.write("{},{}\n".format(fEidx,radiographic_median_SECT))
            with open(stdDevStatFilePath, "a") as stdDevStatFile:
                stdDevStatFile.write("{},{}\n".format(fEidx,radiographic_stdDev_SECT))
            with open(varStatFilePath, "a") as varStatFile:
                varStatFile.write("{},{}\n".format(fEidx,radiographic_variance_SECT))
        elif numDims==3:
            meanArray = numpy.zeros((dArray.shape[-1]), dtype=arrayType)
            medianArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            stdDevArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            varianceArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            # === Open file and write data === #
            meanStatFile = open(meanStatFilePath,"a")
            medianStatFile = open(medianStatFilePath,"a")
            stdDevStatFile = open(stdDevStatFilePath,"a")
            varStatFile = open(varStatFilePath,"a")
            meanStatFile.write("{},".format(fEidx))
            medianStatFile.write("{},".format(fEidx))
            stdDevStatFile.write("{},".format(fEidx))
            varStatFile.write("{},".format(fEidx))
            for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[2]):
                mean = numpy.nanmean(dArray[:,:,channelIdx], None)
                median = numpy.nanmedian(dArray[:,:,channelIdx], None)
                stdDev = numpy.nanstd(dArray[:,:,channelIdx], None)
                variance = numpy.nanvar(dArray[:,:,channelIdx], None)
                meanArray[channelIdx]=mean
                medianArray[channelIdx]=median
                stdDevArray[channelIdx]=stdDev
                varianceArray[channelIdx]=variance
                #print("=== Channel {} ===".format(channelIdx))
                meanStatFile.write("{}".format(mean))
                medianStatFile.write("{}".format(median))
                stdDevStatFile.write("{}".format(stdDev))
                varStatFile.write("{}".format(variance))
                if channelIdx < (dArray.shape[2]-1):
                    meanStatFile.write(",")
                    medianStatFile.write(",")
                    stdDevStatFile.write(",")
                    varStatFile.write(",")
                #print("Mean:\t\t\t{}".format(mean))
                #print("Median:\t\t\t{}".format(median))
                #print("std. Deviation:\t{}".format(stdDev))
                #print("variance\t\t{}:".format(variance))
            meanStatFile.write("\n")
            medianStatFile.write("\n")
            stdDevStatFile.write("\n")
            varStatFile.write("\n")
            meanStatFile.close()
            medianStatFile.close()
            stdDevStatFile.close()
            varStatFile.close()
            radiographic_mean_MECT+=meanArray
            radiographic_median_MECT+=medianArray
            radiographic_stdDev_MECT+=stdDevArray
            radiographic_variance_MECT+=varianceArray
        elif numDims==4:
            meanArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            medianArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            stdDevArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            varianceArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            # === Open file and write data === #
            meanStatFile = open(meanStatFilePath,"a")
            medianStatFile = open(medianStatFilePath,"a")
            stdDevStatFile = open(stdDevStatFilePath,"a")
            varStatFile = open(varStatFilePath,"a")
            meanStatFile.write("{},".format(fEidx))
            medianStatFile.write("{},".format(fEidx))
            stdDevStatFile.write("{},".format(fEidx))
            varStatFile.write("{},".format(fEidx))
            for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[3]):
                mean = numpy.nanmean(dArray[:,:,:,channelIdx], None)
                median = numpy.nanmedian(dArray[:,:,:,channelIdx], None)
                stdDev = numpy.nanstd(dArray[:,:,:,channelIdx], None)
                variance = numpy.nanvar(dArray[:,:,:,channelIdx], None)
                meanArray[channelIdx]=mean
                medianArray[channelIdx]=median
                stdDevArray[channelIdx]=stdDev
                varianceArray[channelIdx]=variance
                #print("=== Channel {} ===".format(channelIdx))
                meanStatFile.write("{}".format(mean))
                medianStatFile.write("{}".format(median))
                stdDevStatFile.write("{}".format(stdDev))
                varStatFile.write("{}".format(variance))
                if channelIdx < (dArray.shape[2]-1):
                    meanStatFile.write(",")
                    medianStatFile.write(",")
                    stdDevStatFile.write(",")
                    varStatFile.write(",")
                #print("Mean:\t\t\t{}".format(mean))
                #print("Median:\t\t\t{}".format(median))
                #print("std. Deviation:\t{}".format(stdDev))
                #print("variance\t\t{}:".format(variance))
            meanStatFile.write("\n")
            medianStatFile.write("\n")
            stdDevStatFile.write("\n")
            varStatFile.write("\n")
            meanStatFile.close()
            medianStatFile.close()
            stdDevStatFile.close()
            varStatFile.close()
            radiographic_mean_MECT+=meanArray
            radiographic_median_MECT+=medianArray
            radiographic_stdDev_MECT+=stdDevArray
            radiographic_variance_MECT+=varianceArray

    radiographic_mean_MECT /= float(len(inputFileArray))
    radiographic_median_MECT /= float(len(inputFileArray))
    radiographic_stdDev_MECT /= float(len(inputFileArray))
    radiographic_variance_MECT /= float(len(inputFileArray))
    radiographic_mean_SECT /= float(len(inputFileArray))
    radiographic_median_SECT /= float(len(inputFileArray))
    radiographic_stdDev_SECT /= float(len(inputFileArray))
    radiographic_variance_SECT /= float(len(inputFileArray))

    if numDims >= 3:
        print("=== Statistics radiographic projections ===")
        print("Mean:\t\t\t{}".format(radiographic_mean_MECT))
        print("Median:\t\t\t{}".format(radiographic_median_MECT))
        print("std. Deviation:\t{}".format(radiographic_stdDev_MECT))
        print("variance:\t\t{}".format(radiographic_variance_MECT))
    else:
        print("=== Statistics radiographic projections ===")
        print("Mean:\t\t\t{}".format(radiographic_mean_SECT))
        print("Median:\t\t\t{}".format(radiographic_median_SECT))
        print("std. Deviation:\t{}".format(radiographic_stdDev_SECT))
        print("variance:\t\t{}".format(radiographic_variance_SECT))

    # =========================== #
    # === second: scattermaps === #
    # =========================== #
    scattering_mean_SECT = 0
    scattering_median_SECT = 0
    scattering_stdDev_SECT = 0
    scattering_variance_SECT = 0
    scattering_mean_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    scattering_median_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    scattering_stdDev_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    scattering_variance_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    meanStatFilePath = os.path.join(outputDir,"meanStat_scatter.csv")
    medianStatFilePath = os.path.join(outputDir,"medianStat_scatter.csv")
    stdDevStatFilePath = os.path.join(outputDir,"stdDevStat_scatter.csv")
    varStatFilePath = os.path.join(outputDir,"varStat_scatter.csv")
    # for fEntry in scatterFileArray:
    for fEidx in range(0, len(scatterFileArray)):
        fEntry = scatterFileArray[fEidx]
        dArray = numpy.zeros((1,1,1))
        f = h5py.File(fEntry,'r')
        dArray = numpy.array(f[fieldName]['value'], order='F').transpose()
        f.close()
        #dArray = numpy.clip(numpy.divide(dArray, flatField), 0.0, 1.0)
        for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[2]):
            minValX = 0
            maxValX = numpy.max(numpy.abs(dArray[:, :, channelIdx]))
            dArray[:, :, channelIdx] = (dArray[:, :, channelIdx] - minValX) / (maxValX - minValX)

        if numDims==2:
            mean = numpy.nanmean(dArray, None)
            median = numpy.nanmedian(dArray, None)
            stdDev = numpy.nanstd(dArray, None)
            variance = numpy.nanvar(dArray, None)
            #print("Mean:\t\t\t{}".format(mean))
            #print("Median:\t\t\t{}".format(median))
            #print("std. Deviation:\t{}".format(stdDev))
            #print("variance\t\t{}:".format(variance))
            scattering_mean_SECT+=mean
            scattering_median_SECT+=median
            scattering_stdDev_SECT+=stdDev
            scattering_variance_SECT+=variance
            with open(meanStatFilePath, "a") as meanStatFile:
                meanStatFile.write("{},{}\n".format(fEidx,radiographic_mean_SECT))
            with open(medianStatFilePath, "a") as medianStatFile:
                medianStatFile.write("{},{}\n".format(fEidx,radiographic_median_SECT))
            with open(stdDevStatFilePath, "a") as stdDevStatFile:
                stdDevStatFile.write("{},{}\n".format(fEidx,radiographic_stdDev_SECT))
            with open(varStatFilePath, "a") as varStatFile:
                varStatFile.write("{},{}\n".format(fEidx,radiographic_variance_SECT))
        elif numDims==3:
            meanArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            medianArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            stdDevArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            varianceArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            # === Open file and write data === #
            meanStatFile = open(meanStatFilePath,"a")
            medianStatFile = open(medianStatFilePath,"a")
            stdDevStatFile = open(stdDevStatFilePath,"a")
            varStatFile = open(varStatFilePath,"a")
            meanStatFile.write("{},".format(fEidx))
            medianStatFile.write("{},".format(fEidx))
            stdDevStatFile.write("{},".format(fEidx))
            varStatFile.write("{},".format(fEidx))
            for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[2]):
                mean = numpy.nanmean(dArray[:,:,channelIdx], None)
                median = numpy.nanmedian(dArray[:,:,channelIdx], None)
                stdDev = numpy.nanstd(dArray[:,:,channelIdx], None)
                variance = numpy.nanvar(dArray[:,:,channelIdx], None)
                meanArray[channelIdx]=mean
                medianArray[channelIdx]=median
                stdDevArray[channelIdx]=stdDev
                varianceArray[channelIdx]=variance
                #print("=== Channel {} ===".format(channelIdx))
                meanStatFile.write("{}".format(mean))
                medianStatFile.write("{}".format(median))
                stdDevStatFile.write("{}".format(stdDev))
                varStatFile.write("{}".format(variance))
                if channelIdx < (dArray.shape[2]-1):
                    meanStatFile.write(",")
                    medianStatFile.write(",")
                    stdDevStatFile.write(",")
                    varStatFile.write(",")
                #print("Mean:\t\t\t{}".format(mean))
                #print("Median:\t\t\t{}".format(median))
                #print("std. Deviation:\t{}".format(stdDev))
                #print("variance\t\t{}:".format(variance))
            meanStatFile.write("\n")
            medianStatFile.write("\n")
            stdDevStatFile.write("\n")
            varStatFile.write("\n")
            meanStatFile.close()
            medianStatFile.close()
            stdDevStatFile.close()
            varStatFile.close()
            scattering_mean_MECT+=meanArray
            scattering_median_MECT+=medianArray
            scattering_stdDev_MECT+=stdDevArray
            scattering_variance_MECT+=varianceArray
        elif numDims==4:
            meanArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            medianArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            stdDevArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            varianceArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            # === Open file and write data === #
            meanStatFile = open(meanStatFilePath,"a")
            medianStatFile = open(medianStatFilePath,"a")
            stdDevStatFile = open(stdDevStatFilePath,"a")
            varStatFile = open(varStatFilePath,"a")
            meanStatFile.write("{},".format(fEidx))
            medianStatFile.write("{},".format(fEidx))
            stdDevStatFile.write("{},".format(fEidx))
            varStatFile.write("{},".format(fEidx))
            for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[3]):
                mean = numpy.nanmean(dArray[:,:,:,channelIdx], None)
                median = numpy.nanmedian(dArray[:,:,:,channelIdx], None)
                stdDev = numpy.nanstd(dArray[:,:,:,channelIdx], None)
                variance = numpy.nanvar(dArray[:,:,:,channelIdx], None)
                meanArray[channelIdx]=mean
                medianArray[channelIdx]=median
                stdDevArray[channelIdx]=stdDev
                varianceArray[channelIdx]=variance
                meanStatFile.write("{}".format(mean))
                medianStatFile.write("{}".format(median))
                stdDevStatFile.write("{}".format(stdDev))
                varStatFile.write("{}".format(variance))
                if channelIdx < (dArray.shape[2]-1):
                    meanStatFile.write(",")
                    medianStatFile.write(",")
                    stdDevStatFile.write(",")
                    varStatFile.write(",")
            meanStatFile.write("\n")
            medianStatFile.write("\n")
            stdDevStatFile.write("\n")
            varStatFile.write("\n")
            meanStatFile.close()
            medianStatFile.close()
            stdDevStatFile.close()
            varStatFile.close()
            scattering_mean_MECT+=meanArray
            scattering_median_MECT+=medianArray
            scattering_stdDev_MECT+=stdDevArray
            scattering_variance_MECT+=varianceArray

    scattering_mean_MECT /= float(len(scatterFileArray))
    scattering_median_MECT /= float(len(scatterFileArray))
    scattering_stdDev_MECT /= float(len(scatterFileArray))
    scattering_variance_MECT /= float(len(scatterFileArray))
    scattering_mean_SECT /= float(len(scatterFileArray))
    scattering_median_SECT /= float(len(scatterFileArray))
    scattering_stdDev_SECT /= float(len(scatterFileArray))
    scattering_variance_SECT /= float(len(scatterFileArray))

    if numDims >= 3:
        print("=== Statistics scattermaps ===")
        print("Mean:\t\t\t{}".format(scattering_mean_MECT))
        print("Median:\t\t\t{}".format(scattering_median_MECT))
        print("std. Deviation:\t{}".format(scattering_stdDev_MECT))
        print("variance:\t\t{}".format(scattering_variance_MECT))
    else:
        print("=== Statistics scattermaps ===")
        print("Mean:\t\t\t{}".format(scattering_mean_SECT))
        print("Median:\t\t\t{}".format(scattering_median_SECT))
        print("std. Deviation:\t{}".format(scattering_stdDev_SECT))
        print("variance:\t\t{}".format(scattering_variance_SECT))

    # ========================================== #
    # === third: tomographic reconstructions === #
    # ========================================== #
    tomographic_mean_SECT = 0
    tomographic_median_SECT = 0
    tomographic_stdDev_SECT = 0
    tomographic_variance_SECT = 0
    tomographic_mean_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    tomographic_median_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    tomographic_stdDev_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    tomographic_variance_MECT = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
    meanStatFilePath = os.path.join(outputDir,"meanStat_tomo.csv")
    medianStatFilePath = os.path.join(outputDir,"medianStat_tomo.csv")
    stdDevStatFilePath = os.path.join(outputDir,"stdDevStat_tomo.csv")
    varStatFilePath = os.path.join(outputDir,"varStat_tomo.csv")
    # for fEntry in observationFileArray:
    for fEidx in range(0, len(observationFileArray)):
        fEntry = observationFileArray[fEidx]
        dArray = numpy.zeros((1,1,1))
        f = h5py.File(fEntry,'r')
        dArray = numpy.array(f[fieldName]['value'], order='F').transpose()
        f.close()
        # no normalisation here, if possible
        #dArray = numpy.clip(numpy.divide(dArray, flatField), 0.0, 1.0)
        #for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[2]):
        #    minValX = 0
        #    maxValX = numpy.max(numpy.abs(dArray[:, :, channelIdx]))
        #    dArray[:, :, channelIdx] = (dArray[:, :, channelIdx] - minValX) / (maxValX - minValX)

        if numDims==2:
            mean = numpy.nanmean(dArray, None)
            median = numpy.nanmedian(dArray, None)
            stdDev = numpy.nanstd(dArray, None)
            variance = numpy.nanvar(dArray, None)
            #print("Mean:\t\t\t{}".format(mean))
            #print("Median:\t\t\t{}".format(median))
            #print("std. Deviation:\t{}".format(stdDev))
            #print("variance\t\t{}:".format(variance))
            with open(meanStatFilePath, "a") as meanStatFile:
                meanStatFile.write("{},{}\n".format(fEidx,radiographic_mean_SECT))
            with open(medianStatFilePath, "a") as medianStatFile:
                medianStatFile.write("{},{}\n".format(fEidx,radiographic_median_SECT))
            with open(stdDevStatFilePath, "a") as stdDevStatFile:
                stdDevStatFile.write("{},{}\n".format(fEidx,radiographic_stdDev_SECT))
            with open(varStatFilePath, "a") as varStatFile:
                varStatFile.write("{},{}\n".format(fEidx,radiographic_variance_SECT))
            tomographic_mean_SECT+=mean
            tomographic_median_SECT+=median
            tomographic_stdDev_SECT+=stdDev
            tomographic_variance_SECT+=variance
        elif numDims==3:
            meanArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            medianArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            stdDevArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            varianceArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            # === Open file and write data === #
            meanStatFile = open(meanStatFilePath,"a")
            medianStatFile = open(medianStatFilePath,"a")
            stdDevStatFile = open(stdDevStatFilePath,"a")
            varStatFile = open(varStatFilePath,"w")
            meanStatFile.write("{},".format(fEidx))
            medianStatFile.write("{},".format(fEidx))
            stdDevStatFile.write("{},".format(fEidx))
            varStatFile.write("{},".format(fEidx))
            for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[2]):
                mean = numpy.nanmean(dArray[:,:,channelIdx], None)
                median = numpy.nanmedian(dArray[:,:,channelIdx], None)
                stdDev = numpy.nanstd(dArray[:,:,channelIdx], None)
                variance = numpy.nanvar(dArray[:,:,channelIdx], None)
                meanArray[channelIdx]=mean
                medianArray[channelIdx]=median
                stdDevArray[channelIdx]=stdDev
                varianceArray[channelIdx]=variance
                #print("=== Channel {} ===".format(channelIdx))
                meanStatFile.write("{}".format(mean))
                medianStatFile.write("{}".format(median))
                stdDevStatFile.write("{}".format(stdDev))
                varStatFile.write("{}".format(variance))
                if channelIdx < (dArray.shape[2]-1):
                    meanStatFile.write(",")
                    medianStatFile.write(",")
                    stdDevStatFile.write(",")
                    varStatFile.write(",")
                #print("Mean:\t\t\t{}".format(mean))
                #print("Median:\t\t\t{}".format(median))
                #print("std. Deviation:\t{}".format(stdDev))
                #print("variance\t\t{}:".format(variance))
            meanStatFile.write("\n")
            medianStatFile.write("\n")
            stdDevStatFile.write("\n")
            varStatFile.write("\n")
            meanStatFile.close()
            medianStatFile.close()
            stdDevStatFile.close()
            varStatFile.close()
            tomographic_mean_MECT+=meanArray
            tomographic_median_MECT+=medianArray
            tomographic_stdDev_MECT+=stdDevArray
            tomographic_variance_MECT+=varianceArray
        elif numDims==4:
            meanArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            medianArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            stdDevArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            varianceArray = numpy.zeros((dArray.shape[-1],), dtype=arrayType)
            # === Open file and write data === #
            meanStatFile = open(meanStatFilePath,"a")
            medianStatFile = open(medianStatFilePath,"a")
            stdDevStatFile = open(stdDevStatFilePath,"a")
            varStatFile = open(varStatFilePath,"a")
            meanStatFile.write("{},".format(fEidx))
            medianStatFile.write("{},".format(fEidx))
            stdDevStatFile.write("{},".format(fEidx))
            varStatFile.write("{},".format(fEidx))
            for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[3]):
                mean = numpy.nanmean(dArray[:,:,:,channelIdx], None)
                median = numpy.nanmedian(dArray[:,:,:,channelIdx], None)
                stdDev = numpy.nanstd(dArray[:,:,:,channelIdx], None)
                variance = numpy.nanvar(dArray[:,:,:,channelIdx], None)
                meanArray[channelIdx]=mean
                medianArray[channelIdx]=median
                stdDevArray[channelIdx]=stdDev
                varianceArray[channelIdx]=variance
                meanStatFile.write("{}".format(mean))
                medianStatFile.write("{}".format(median))
                stdDevStatFile.write("{}".format(stdDev))
                varStatFile.write("{}".format(variance))
                if channelIdx < (dArray.shape[2]-1):
                    meanStatFile.write(",")
                    medianStatFile.write(",")
                    stdDevStatFile.write(",")
                    varStatFile.write(",")
            meanStatFile.write("\n")
            medianStatFile.write("\n")
            stdDevStatFile.write("\n")
            varStatFile.write("\n")
            meanStatFile.close()
            medianStatFile.close()
            stdDevStatFile.close()
            varStatFile.close()
            tomographic_mean_MECT+=meanArray
            tomographic_median_MECT+=medianArray
            tomographic_stdDev_MECT+=stdDevArray
            tomographic_variance_MECT+=varianceArray

    tomographic_mean_MECT /= float(len(observationFileArray))
    tomographic_median_MECT /= float(len(observationFileArray))
    tomographic_stdDev_MECT /= float(len(observationFileArray))
    tomographic_variance_MECT /= float(len(observationFileArray))
    tomographic_mean_SECT /= float(len(observationFileArray))
    tomographic_median_SECT /= float(len(observationFileArray))
    tomographic_stdDev_SECT /= float(len(observationFileArray))
    tomographic_variance_SECT /= float(len(observationFileArray))

    if numDims >= 3:
        print("=== Statistics tomographic reconstructions ===")
        print("Mean:\t\t\t{}".format(tomographic_mean_MECT))
        print("Median:\t\t\t{}".format(tomographic_median_MECT))
        print("std. Deviation:\t{}".format(tomographic_stdDev_MECT))
        print("variance:\t\t{}".format(tomographic_variance_MECT))
    else:
        print("=== Statistics tomographic reconstructions ===")
        print("Mean:\t\t\t{}".format(tomographic_mean_SECT))
        print("Median:\t\t\t{}".format(tomographic_median_SECT))
        print("std. Deviation:\t{}".format(tomographic_stdDev_SECT))
        print("variance:\t\t{}".format(tomographic_variance_SECT))