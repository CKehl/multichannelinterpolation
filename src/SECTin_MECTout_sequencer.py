"""
Created on Aug 30 2018

@author: chke (Christian Kehl)
"""

import numpy
# import threading
import multiprocessing
import ctypes
from scipy import linalg
from scipy import ndimage
from keras.preprocessing.image import load_img, img_to_array, array_to_img  # , save_img
from keras.utils import Sequence
import glob
# from PIL import Image
from skimage import util
from skimage import transform
#from skimage.filters.rank import median
#from skimage.morphology import disk
import itertools
import os
import re
import h5py

WORKERS = 12
CACHE_SIZE = 32
dims = (256, 256, 1)
NORM_DATA_MODE = 0  # 0 - per image over all channels; 1 - per image per channel; 2 - flat-field norm
TYPES = {"XRAY": 0, "SCATTER": 1, "CT": 2}



def numpy_normalize(v):
    norm = numpy.linalg.norm(v)
    if norm == 0:
        return v
    return v/norm

def normaliseFieldArray(a, numChannels, flatField=None, itype=TYPES["XRAY"]):
    minx = None
    maxx = None
    if itype == TYPES['XRAY']:
        if flatField!=None:
            a = numpy.clip(numpy.divide(a, flatField), 0.0, 1.0)
        else:
            if numChannels<=1:
                minx = numpy.min(a)
                maxx = numpy.max(a)
                a = (a - minx) / (maxx - minx)
            else:
                minx = []
                maxx = []
                for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                    minx.append(numpy.min(a[:, :, channelIdx]))
                    maxx.append(numpy.max(a[:, :, channelIdx]))
                    a[:, :, channelIdx] = (a[:, :, channelIdx] - minx[channelIdx]) / (maxx[channelIdx] - minx[channelIdx])
    elif itype == TYPES['SCATTER']:
        if numChannels<=1:
            minx = 0
            maxx = numpy.max(numpy.abs(a))
            a = (a-minx)/(maxx-minx)
        else:
            minx = []
            maxx = []
            for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                #minx.append(numpy.min(flatField[:, :, channelIdx]))
                #maxx.append(numpy.max(flatField[:, :, channelIdx]))
                minx.append(0)
                maxx.append(numpy.max(numpy.abs(a[:, :, channelIdx])))
                a[:, :, channelIdx] = (a[:, :, channelIdx] - minx[channelIdx]) / (maxx[channelIdx] - minx[channelIdx])
    elif itype == TYPES['CT']:
        if numChannels<=1:
            minx = numpy.min(a)
            maxx = numpy.max(a)
            a = (a - minx) / (maxx-minx)
        else:
            minx = []
            maxx = []
            for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                minx.append(numpy.min(a[:, :, channelIdx]))
                maxx.append(numpy.max(a[:, :, channelIdx]))
                a[:, :, channelIdx] = (a[:, :, channelIdx] - minx[channelIdx]) / (maxx[channelIdx] - minx[channelIdx])
    return minx, maxx, a

def denormaliseFieldArray(a, numChannels, minx=None, maxx=None, flatField=None, itype=TYPES["XRAY"]):
    if itype == TYPES['XRAY']:
        if flatField != None:
            a = a * flatField
        else:
            if numChannels <= 1:
                a = a * (maxx - minx) + minx
            else:
                for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                    a[:, :, channelIdx] = a[:, :, channelIdx] * (maxx[channelIdx] - minx[channelIdx]) + minx[channelIdx]
    elif itype == TYPES['SCATTER']:
        if numChannels <= 1:
            a = a*(maxx-minx)+minx
        else:
            for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                a[:, :, channelIdx] = a[:, :, channelIdx] * (maxx[channelIdx] - minx[channelIdx]) + minx[channelIdx]
    elif itype == TYPES['CT']:
        if numChannels <= 1:
            a = a * (maxx - minx) + minx
        else:
            for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                a[:, :, channelIdx] = a[:, :, channelIdx] * (maxx[channelIdx] - minx[channelIdx]) + minx[channelIdx]
    return a



class MECTsequencer(Sequence):

    def __init__(self, batch_size=1, image_size=(128, 128), input_channels=32, targetSize=(128, 128), output_channels=1, useResize=False, useCrop=False,
                 useZoom=False, zoomFactor=1.0, useAWGN=False, useMedian=False, useGaussian=False, useFlipping=False, useNormData=False, cache_period=512,
                 save_to_dir=None, save_format="png", threadLockVar=None, useCache=False):
        self.x_type = TYPES["SCATTER"]
        self.y_type = TYPES["SCATTER"]
        if self.x_type==TYPES["XRAY"]:
            # XRAY
            self.MECTnoise_mu = numpy.array([0.26358,0.24855,0.23309,0.22195,0.21639,0.21285,0.21417,0.21979,0.22502,0.23387,0.24120,0.24882,0.25177,0.25594,0.26005,0.26350,0.27067,0.27440,0.27284,0.26868,0.26477,0.25461,0.24436,0.24287,0.23849,0.24022,0.23915,0.23874,0.23968,0.23972,0.24100,0.23973,0.23921,0.24106,0.24177,0.24155,0.24358,0.24578,0.24682,0.24856,0.24969,0.25206,0.25337,0.25650,0.25627,0.25921,0.26303,0.26615,0.26772,0.26882,0.27248,0.27400,0.27722,0.27905,0.28138,0.28406,0.28593,0.28830,0.29129,0.29420,0.29673,0.29776,0.29955,0.30050,0.30151,0.30196,0.30340,0.30282,0.30546,0.30509,0.30569,0.30667,0.30512,0.30413,0.30496,0.30474,0.30525,0.30534,0.30503,0.30635,0.30539,0.30561,0.30660,0.30491,0.30486,0.30291,0.30323,0.30253,0.29960,0.29734,0.29760,0.29464,0.29273,0.29035,0.28906,0.28680,0.28446,0.27905,0.27842,0.27555,0.27112,0.26879,0.26760,0.26547,0.26289,0.25914,0.25776,0.25641,0.25394,0.25148,0.25033,0.24752,0.24648,0.24424,0.24386,0.24097,0.24095,0.24104,0.24090,0.23948,0.23985,0.23916,0.23931,0.23869,0.23922,0.23671,0.23994,0.24009,0.24299,0.25392,0.26096,0.26740,0.27136,0.27207,0.27209,0.26671,0.26037,0.25427,0.25223,0.25006,0.24506,0.23531,0.22816,0.21955,0.21713,0.21705,0.22167,0.23419,0.24789,0.26416], dtype=numpy.float32)
            self.MECTnoise_sigma = numpy.array([0.03491,0.02537,0.01526,0.00798,0.00368,0.00220,0.00389,0.00819,0.01553,0.02466,0.03281,0.03765,0.04221,0.04212,0.03958,0.03447,0.02916,0.02766,0.02757,0.02671,0.02047,0.01121,0.00309,0.00321,0.00397,0.00433,0.00456,0.00514,0.00598,0.00674,0.00784,0.00852,0.00934,0.01040,0.01126,0.01257,0.01349,0.01460,0.01611,0.01770,0.01930,0.02084,0.02267,0.02354,0.02597,0.02677,0.02758,0.02859,0.03009,0.03126,0.03121,0.03174,0.03198,0.03140,0.03225,0.03150,0.03105,0.03154,0.03063,0.02965,0.02933,0.02837,0.02748,0.02662,0.02632,0.02540,0.02497,0.02515,0.02463,0.02431,0.02462,0.02559,0.02677,0.02757,0.02812,0.02728,0.02712,0.02635,0.02568,0.02622,0.02636,0.02611,0.02635,0.02649,0.02604,0.02533,0.02588,0.02643,0.02724,0.02824,0.02925,0.02916,0.02922,0.03064,0.03059,0.03050,0.03066,0.03251,0.03196,0.03219,0.03295,0.03199,0.03130,0.02980,0.02977,0.02886,0.02701,0.02579,0.02406,0.02252,0.02103,0.01931,0.01750,0.01566,0.01390,0.01238,0.01035,0.00918,0.00798,0.00687,0.00606,0.00523,0.00467,0.00423,0.00397,0.00430,0.00411,0.00344,0.00222,0.00929,0.01874,0.02600,0.02750,0.02828,0.02741,0.03276,0.03759,0.04272,0.04187,0.03968,0.03494,0.02768,0.01931,0.01083,0.00532,0.00377,0.00777,0.01520,0.02568,0.03590], dtype=numpy.float32)
        elif self.x_type==TYPES["SCATTER"]:
            # SCATTER
            self.MECTnoise_mu = numpy.array([0.16866670,0.17188519,0.17515915,0.17835625,0.18134183,0.18390702,0.18609122,0.18790514,0.18933285,0.19041915,0.19118198,0.19161633,0.19181411,0.19186348,0.19185040,0.19184256,0.19188470,0.19197313,0.19209580,0.19224754,0.19241207,0.19258621,0.19275952,0.19290241,0.19301524,0.19307717,0.19306810,0.19298917,0.19281435,0.19255346,0.19221039,0.19176093,0.19122621,0.19061593,0.18991660,0.18913179,0.18829705,0.18740011,0.18644242,0.18542260,0.18435654,0.18325110,0.18211837,0.18094630,0.17975648,0.17856240,0.17736573,0.17617767,0.17500219,0.17385002,0.17272437,0.17162455,0.17057456,0.16956633,0.16860679,0.16770124,0.16684261,0.16603374,0.16528716,0.16458883,0.16394797,0.16335864,0.16282625,0.16234653,0.16191163,0.16152307,0.16117932,0.16088236,0.16062331,0.16040275,0.16022629,0.16007563,0.15996251,0.15987573,0.15982041,0.15979193,0.15978788,0.15981399,0.15986311,0.15994251,0.16005459,0.16019302,0.16036940,0.16058268,0.16083441,0.16112886,0.16147478,0.16187346,0.16231956,0.16282015,0.16338502,0.16399501,0.16466099,0.16538820,0.16617289,0.16701784,0.16791800,0.16887256,0.16987358,0.17093329,0.17204428,0.17319362,0.17438019,0.17559014,0.17682701,0.17806635,0.17931469,0.18056600,0.18180696,0.18301938,0.18419762,0.18533589,0.18641122,0.18743520,0.18839959,0.18929145,0.19010590,0.19084049,0.19149243,0.19206308,0.19254355,0.19291977,0.19319933,0.19338428,0.19347334,0.19348168,0.19342172,0.19330871,0.19316624,0.19299663,0.19281704,0.19264048,0.19248135,0.19235786,0.19225717,0.19219786,0.19214044,0.19203097,0.19180060,0.19132749,0.19051121,0.18933457,0.18781172,0.18595189,0.18373764,0.18111646,0.17815353,0.17494756,0.17167550,0.16848442], dtype=numpy.float32)
            self.MECTnoise_sigma = numpy.array([0.03945668,0.03752889,0.03544898,0.03323879,0.03096720,0.02856092,0.02612186,0.02376646,0.02169589,0.02004326,0.01889315,0.01825106,0.01797775,0.01793787,0.01798946,0.01807670,0.01817564,0.01827498,0.01837080,0.01845804,0.01854509,0.01863035,0.01872147,0.01882850,0.01894833,0.01909700,0.01926717,0.01944903,0.01965817,0.01988674,0.02013096,0.02037972,0.02064439,0.02092013,0.02119017,0.02143954,0.02169283,0.02193263,0.02214348,0.02231043,0.02243537,0.02251225,0.02254155,0.02250185,0.02240452,0.02225120,0.02204627,0.02180226,0.02151376,0.02118313,0.02082264,0.02043567,0.02004489,0.01964261,0.01923366,0.01882669,0.01841781,0.01800790,0.01761821,0.01723964,0.01688061,0.01654472,0.01623485,0.01595351,0.01568520,0.01544452,0.01522702,0.01503263,0.01486395,0.01472051,0.01461037,0.01451426,0.01445295,0.01441048,0.01440137,0.01441387,0.01444312,0.01449269,0.01456381,0.01465286,0.01476782,0.01489762,0.01505751,0.01523783,0.01543211,0.01564785,0.01588430,0.01613858,0.01641310,0.01670377,0.01701734,0.01733008,0.01765341,0.01798678,0.01833384,0.01867848,0.01902556,0.01936452,0.01968341,0.02000430,0.02031607,0.02061045,0.02088021,0.02110307,0.02129998,0.02145134,0.02156904,0.02166331,0.02172182,0.02173031,0.02169494,0.02162645,0.02150349,0.02133820,0.02113991,0.02091581,0.02066636,0.02039382,0.02011436,0.01984492,0.01957840,0.01930333,0.01905621,0.01882268,0.01862423,0.01844558,0.01829097,0.01816721,0.01807584,0.01799463,0.01795434,0.01791797,0.01787606,0.01783821,0.01779470,0.01775097,0.01772077,0.01774389,0.01789813,0.01839385,0.01945910,0.02104492,0.02306657,0.02539757,0.02782046,0.03022519,0.03256747,0.03480967,0.03695302,0.03895815], dtype=numpy.float32)

        self.batch_size = batch_size
        self.image_size = image_size
        self.target_size = targetSize
        self.input_channels = input_channels
        self.output_channels = output_channels
        self.useResize = useResize
        self.useCrop = useCrop
        self.zoomFactor = zoomFactor
        self.useZoom = useZoom
        self.useFlipping = useFlipping
        self.useAWGN = useAWGN
        self.useMedian = useMedian
        self.medianSize = [0,1,3,5,7,9,11]
        self.useGaussian = useGaussian
        self.gaussianRange = (0, 0.075)
        self.useNormData = useNormData
        # ========================================#
        # == zoom-related image information ==#
        # ========================================#
        self.im_center = numpy.array(
            [int(self.image_size[0] * self.zoomFactor - 1) / 2, int(self.image_size[1] * self.zoomFactor - 1) / 2],
            dtype=numpy.int32)
        self.im_shift = numpy.array([(self.image_size[0] - 1) / 2, (self.image_size[1] - 1) / 2], dtype=numpy.int32)
        left = self.im_center[0] - self.im_shift[0]
        right = left + self.image_size[0]
        top = self.im_center[1] - self.im_shift[1]
        bottom = top + self.image_size[1]
        self.im_bounds = (left, right, top, bottom)
        # ===================================#
        # == directory-related information ==#
        # ===================================#
        self.inputFileArray = []
        #self.scatterMapArray = []
        #self.observeFileArray = []
        self.outputFileArray = []
        self.save_to_dir = save_to_dir
        self.save_format = save_format
        # ==================================#
        # == flat-field related variables ==#
        # ==================================#
        self.flatField_input = None
        self.flatField_output = None
        # ===============================#
        # == caching-related variables ==#
        # ===============================#
        self.useCache = useCache
        self.cache_size = CACHE_SIZE
        self.cache_period = cache_period
        self.cacheX = numpy.zeros(1, dtype=numpy.float32)
        self.cacheY = numpy.zeros(1, dtype=numpy.float32)
        self.renew_cache = multiprocessing.Value(ctypes.c_bool, False)
        self.cacheUsed_counter = multiprocessing.Value('i', 0)
        self.cacheRenewed_counter = multiprocessing.Value('i', 0)
        self._lock_ = threadLockVar
        self._memlock_ = multiprocessing.Lock()
        self._refreshEvent_ = multiprocessing.Event()

        self.batch_image_size_X = (self.batch_size, self.image_size[0], self.image_size[1], self.input_channels)
        self.batch_image_size_Y = (self.batch_size, self.image_size[0], self.image_size[1], self.output_channels)
        if self.useCrop or self.useResize:
            self.batch_image_size_X = (self.batch_size, self.target_size[0], self.target_size[1], self.input_channels)
            self.batch_image_size_Y = (self.batch_size, self.target_size[0], self.target_size[1], self.output_channels)

    def prepareDirectFileInput(self, input_image_paths, output_image_paths, flatFieldFilePath=None):
        for entry in input_image_paths:
            if self.x_type==TYPES["XRAY"]:
                for name in glob.glob(os.path.join(entry, '*X*.h5')):
                    self.inputFileArray.append(name)
            elif self.x_type==TYPES["SCATTER"]:
                for name in glob.glob(os.path.join(entry, '*Y*.h5')):
                    self.inputFileArray.append(name)
        for entry in output_image_paths:
            if self.y_type==TYPES["XRAY"]:
                for name in glob.glob(os.path.join(entry, '*X*.h5')):
                    self.outputFileArray.append(name)
            elif self.y_type==TYPES["SCATTER"]:
                for name in glob.glob(os.path.join(entry, '*Y*.h5')):
                    self.outputFileArray.append(name)
        digits = re.compile(r'(\d+)')
        def tokenize(filename):
            return tuple(int(token) if match else token for token, match in
                         ((fragment, digits.search(fragment)) for fragment in digits.split(filename)))
        # = Now you can sort your file names like so: =#
        self.inputFileArray.sort(key=tokenize)
        self.outputFileArray.sort(key=tokenize)
        self.numImages = len(self.inputFileArray)

        # === prepare image sizes === #
        inImgDims = (self.image_size[0], self.image_size[1], self.input_channels)
        outImgDims = (self.image_size[0], self.image_size[1], self.output_channels)
        f = h5py.File(self.inputFileArray[0], 'r')
        imX = numpy.array(f['data']['value'])
        f.close()
        if len(imX.shape) < 3:
            imX = imX.reshape(imX.shape + (1,))
        if imX.shape != inImgDims:
            print("Error - read data shape and expected data shape of X are not equal. EXITING ...")
            exit()
        f = h5py.File(self.outputFileArray[0], 'r')
        imY = numpy.array(f['data']['value'])
        f.close()
        if len(imY.shape) < 3:
            imY = imY.reshape(imY.shape + (1,))
        if imY.shape != outImgDims:
            print("Error - read data shape {} and expected data shape of Y are not equal. EXITING ...".format(imY.shape))
            exit()
        # === prepare caching === #
        if self.useCache:
            self.cacheX = numpy.zeros((self.cache_size, inImgDims[0], inImgDims[1], inImgDims[2]), dtype=numpy.float32)
            self.cacheY = numpy.zeros((self.cache_size, outImgDims[0], outImgDims[1], outImgDims[2]), dtype=numpy.float32)
            self.renew_cache = multiprocessing.Value(ctypes.c_bool, False)
            self.cacheUsed_counter = multiprocessing.Value('i', 0)
            self.__initCache_open_()
        # === prepare flat-field normalization === #
        if (flatFieldFilePath != None) and (len(flatFieldFilePath) > 0):
            f = h5py.File(flatFieldFilePath, 'r')
            self.flatField_output = numpy.array(f['data']['value'])  # f['data0']
            f.close()
            self.flatField_input = numpy.array(self.flatField_input)
            if self.output_channels==1:
                #self.flatField_output = numpy.sum(self.flatField_output,2)
                self.flatField_input = numpy.mean(self.flatField_output, 2)
            NORM_DATA_MODE = 2

    def _initCache_locked_(self):
        startId = 0
        loadData_flag = True

        with self._lock_:
            startId = self.cacheRenewed_counter.value
            if (startId >= self.cache_size):
                loadData_flag = False
            else:
                loadData_flag = True
                self.cacheRenewed_counter.value += self.batch_size

        if loadData_flag == True:
            # ---------------- #
            # repopulate cache #
            # ---------------- #
            idxArray = numpy.random.randint(0, self.numImages, self.cache_size)
            for ii in itertools.islice(itertools.count(), startId, min([startId + self.batch_size, self.cache_size])):
                imgIndex = idxArray[ii]
                inName = self.inputFileArray[imgIndex]
                outName = self.outputFileArray[imgIndex]
                f = h5py.File(inName, 'r')
                imX = numpy.array(f['data']['value'])
                f.close()
                if len(imX.shape) < 3:
                    imX = imX.reshape(imX.shape + (1,))

                f = h5py.File(outName, 'r')
                imY = numpy.array(f['data']['value'])
                f.close()
                if len(imY.shape) < 3:
                    imY = imY.reshape(imY.shape + (1,))
                #if imX.shape != imY.shape:
                #    raise RuntimeError("Input- and Output sizes do not match.")

                # == Note: do data normalization here to reduce memory footprint ==#
                """
                Data Normalisation
                """
                if self.useNormData:
                    minValX, maxValX, imX = normaliseFieldArray(imX, self.input_channels, self.flatField_input, self.x_type)
                    minValY, maxValY, imY = normaliseFieldArray(imY, self.output_channels, self.flatField_output, self.y_type)

                    #if NORM_DATA_MODE == 0:
                    #    minValX = numpy.min(imX)
                    #    maxValX = numpy.max(imX)
                    #    imX = (imX - minValX) / (maxValX - minValX)
                    #    imX = imX.astype(numpy.float32)
                    #    minValY = numpy.min(imY)
                    #    maxValY = numpy.max(imY)
                    #    imY = (imY - minValY) / (maxValY - minValY)
                    #    imY = imY.astype(numpy.float32)
                    #elif NORM_DATA_MODE == 1:
                    #    for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #        minValX = numpy.min(imX[:, :, channelIdx])
                    #        maxValX = numpy.max(imX[:, :, channelIdx])
                    #        imX[:, :, channelIdx] = (imX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    for channelIdx in itertools.islice(itertools.count(), 0, self.output_channels):
                    #        minValY = numpy.min(imY[:, :, channelIdx])
                    #        maxValY = numpy.max(imY[:, :, channelIdx])
                    #        imY[:, :, channelIdx] = (imY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #    imX = imX.astype(numpy.float32)
                    #    imY = imY.astype(numpy.float32)
                    #elif NORM_DATA_MODE == 2:
                    #    # X-NORM
                    #    if self.x_type == TYPES['XRAY']:
                    #        imX = numpy.clip(numpy.divide(imX, self.flatField_input), 0.0, 1.0)
                    #    elif self.x_type == TYPES['SCATTER']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #            #minValX = numpy.min(self.flatField_input[:, :, channelIdx])
                    #            #maxValX = numpy.max(self.flatField_input[:, :, channelIdx])
                    #            #imX[:, :, channelIdx] = (imX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #            minValX = 0
                    #            maxValX = numpy.max(numpy.abs(imX[:, :, channelIdx]))
                    #            imX[:, :, channelIdx] = (imX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    elif self.x_type == TYPES['CT']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #            minValX = numpy.min(imX[:, :, channelIdx])
                    #            maxValX = numpy.max(imX[:, :, channelIdx])
                    #            imX[:, :, channelIdx] = (imX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    # ==== Y-NORM ==== #
                    #    if self.y_type == TYPES['XRAY']:
                    #        imY = numpy.clip(numpy.divide(imY, self.flatField_output), 0.0, 1.0)
                    #    elif self.y_type == TYPES['SCATTER']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #            #minValY = numpy.min(self.flatField_output[:, :, channelIdx])
                    #            #maxValY = numpy.max(self.flatField_output[:, :, channelIdx])
                    #            #imY[:, :, channelIdx] = (imY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #            minValY = 0
                    #            maxValY = numpy.max(numpy.abs(imY[:, :, channelIdx]))
                    #            imY[:, :, channelIdx] = (imY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #    elif self.y_type == TYPES['CT']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.output_channels):
                    #            minValY = numpy.min(imY[:, :, channelIdx])
                    #            maxValY = numpy.max(imY[:, :, channelIdx])
                    #            imY[:, :, channelIdx] = (imY[:, :, channelIdx] - minValY) / (maxValY - minValY)

                    imX = imX.astype(numpy.float32)
                    imY = imY.astype(numpy.float32)
                with self._memlock_:
                    self.cacheX[ii] = imX
                    self.cacheY[ii] = imY
        else:
            return

        with self._lock_:
            if self.cacheRenewed_counter.value >= self.cache_size:
                self.cacheRenewed_counter.value = 0

            if self.cacheUsed_counter.value >= self.cache_period:
                self.cacheUsed_counter.value = 0

            if ((startId + self.batch_size) >= self.cache_size):
                self.renew_cache.value = False
        return

    def __initCache_open_(self):
        if self.useCache:
            # ---------------- #
            # repopulate cache #
            # ---------------- #
            idxArray = numpy.random.randint(0, self.numImages, self.cache_size)
            for ii in itertools.islice(itertools.count(), 0, self.cache_size):
                # imgIndex = numpy.random.randint(0, self.numImages-1)
                imgIndex = idxArray[ii]
                inName = self.inputFileArray[imgIndex]
                outName = self.outputFileArray[imgIndex]
                f = h5py.File(inName, 'r')
                outImgX = numpy.array(f['data']['value'])
                f.close()
                if len(outImgX.shape) < 3:
                    outImgX = outImgX.reshape(outImgX.shape + (1,))

                f = h5py.File(outName, 'r')
                outImgY = numpy.array(f['data']['value'])
                f.close()
                if len(outImgY.shape) < 3:
                    outImgY = outImgY.reshape(outImgY.shape + (1,))
                #if outImgX.shape != outImgY.shape:
                #    raise RuntimeError("Input- and Output sizes do not match.")

                # == Note: do data normalization here to reduce memory footprint ==#
                """
                Data Normalisation
                """
                if self.useNormData:
                    minValX, maxValX, outImgX = normaliseFieldArray(outImgX, self.input_channels, self.flatField_input, self.x_type)
                    minValY, maxValY, outImgY = normaliseFieldArray(outImgY, self.output_channels, self.flatField_output, self.y_type)

                    #if NORM_DATA_MODE == 0:
                    #    minValX = numpy.min(outImgX)
                    #    maxValX = numpy.max(outImgX)
                    #    outImgX = (outImgX - minValX) / (maxValX - minValX)
                    #    outImgX = outImgX.astype(numpy.float32)
                    #    minValY = numpy.min(outImgY)
                    #    maxValY = numpy.max(outImgY)
                    #    outImgY = (outImgY - minValY) / (maxValY - minValY)
                    #    outImgY = outImgY.astype(numpy.float32)
                    #elif NORM_DATA_MODE == 1:
                    #    for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #        minValX = numpy.min(outImgX[:, :, channelIdx])
                    #        maxValX = numpy.max(outImgX[:, :, channelIdx])
                    #        outImgX[:, :, channelIdx] = (outImgX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    for channelIdx in itertools.islice(itertools.count(), 0, self.output_channels):
                    #        minValY = numpy.min(outImgY[:, :, channelIdx])
                    #        maxValY = numpy.max(outImgY[:, :, channelIdx])
                    #        outImgY[:, :, channelIdx] = (outImgY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #    outImgX = outImgX.astype(numpy.float32)
                    #    outImgY = outImgY.astype(numpy.float32)
                    #elif NORM_DATA_MODE == 2:
                    #    # ==== X-NORM ==== #
                    #    if self.x_type == TYPES['XRAY']:
                    #        outImgX = numpy.clip(numpy.divide(outImgX, self.flatField_input), 0.0, 1.0)
                    #    elif self.x_type == TYPES['SCATTER']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #            #minValX = numpy.min(self.flatField_input[:, :, channelIdx])
                    #            #maxValX = numpy.max(self.flatField_input[:, :, channelIdx])
                    #            #outImgX[:, :, channelIdx] = (outImgX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #            minValX = 0
                    #            maxValX = numpy.max(numpy.abs(outImgX[:, :, channelIdx]))
                    #            outImgX[:, :, channelIdx] = (outImgX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    elif self.x_type == TYPES['CT']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #            minValX = numpy.min(outImgX[:, :, channelIdx])
                    #            maxValX = numpy.max(outImgX[:, :, channelIdx])
                    #            outImgX[:, :, channelIdx] = (outImgX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    # ==== Y-NORM ==== #
                    #    if self.y_type == TYPES['XRAY']:
                    #        outImgY = numpy.clip(numpy.divide(outImgY, self.flatField_output), 0.0, 1.0)
                    #    elif self.y_type == TYPES['SCATTER']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.output_channels):
                    #            #minValY = numpy.min(self.flatField_output[:, :, channelIdx])
                    #            #maxValY = numpy.max(self.flatField_output[:, :, channelIdx])
                    #            #outImgY[:, :, channelIdx] = (outImgY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #            minValY = 0
                    #            maxValY = numpy.max(numpy.abs(outImgY[:, :, channelIdx]))
                    #            outImgY[:, :, channelIdx] = (outImgY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #    elif self.y_type == TYPES['CT']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.output_channels):
                    #            minValY = numpy.min(outImgY[:, :, channelIdx])
                    #            maxValY = numpy.max(outImgY[:, :, channelIdx])
                    #            outImgY[:, :, channelIdx] = (outImgY[:, :, channelIdx] - minValY) / (maxValY - minValY)

                    outImgX = outImgX.astype(numpy.float32)
                    outImgY = outImgY.astype(numpy.float32)

                with self._memlock_:
                    self.cacheX[ii, :, :, :] = outImgX
                    self.cacheY[ii, :, :, :] = outImgY

    def __len__(self):
        return int(numpy.ceil(len(self.inputFileArray) / float(self.batch_size)))

    def __getitem__(self, idx):
        if self.useCache:
            flushCache = False
            with self._lock_:
                flushCache = self.renew_cache.value
            if flushCache == True:
                self._initCache_locked_()

        batchX = numpy.zeros(self.batch_image_size_X, dtype=numpy.float32)
        batchY = numpy.zeros(self.batch_image_size_Y, dtype=numpy.float32)
        idxArray = numpy.random.randint(0, self.cache_size, self.batch_size)
        #imgStartId = (idx*self.batch_size) % (self.cache_size)
        #imgEndId = ((idx+1)*self.batch_size) % (self.cache_size)
        #print("ProcID: {}, minId: {}, maxId: {}".format(idx, imgStartId, imgEndId))
        #idxArray = numpy.arange(imgStartId,imgEndId)
        #if imgStartId>imgEndId:
        #    idxArray = numpy.concatenate(numpy.arange(imgStartId,self.cache_size),numpy.arange(0,imgEndId),axis=0)
        #print("ProcID: {}, imgRange: {}".format(idx,idxArray))
        for j in itertools.islice(itertools.count(), 0, self.batch_size):
            outImgX = None
            outImgY = None
            if self.useCache:
                # imgIndex = numpy.random.randint(0, self.cache_size)
                imgIndex = idxArray[j]
                with self._memlock_:
                    outImgX = self.cacheX[imgIndex]
                    outImgY = self.cacheY[imgIndex]
            else:
                # imgIndex = min([(idx*self.batch_size)+j, self.numImages-1,len(self.inputFileArray)-1])
                imgIndex = ((idx * self.batch_size) + j) % (self.numImages - 1)
                """
                Load data from disk
                """
                inName = self.inputFileArray[imgIndex]
                outName = self.outputFileArray[imgIndex]
                f = h5py.File(inName, 'r')
                outImgX = numpy.array(f['data']['value'])
                f.close()
                if len(outImgX.shape) < 3:
                    outImgX = outImgX.reshape(outImgX.shape + (1,))
                f = h5py.File(outName, 'r')
                outImgY = numpy.array(f['data']['value'])
                f.close()
                if len(outImgY.shape) < 3:
                    outImgY = outImgY.reshape(outImgY.shape + (1,))
                if outImgX.shape != outImgY.shape:
                    raise RuntimeError("Input- and Output sizes do not match.")
                # == Note: do data normalization here to reduce memory footprint ==#
                """
                Data Normalisation
                """
                if self.useNormData:
                    minValX,maxValX,outImgX = normaliseFieldArray(outImgX, self.input_channels, self.flatField_input, self.x_type)
                    minValY,minValY,outImgY = normaliseFieldArray(outImgY, self.output_channels, self.flatField_output, self.y_type)
                    #if NORM_DATA_MODE == 0:
                    #    minValX = numpy.min(outImgX)
                    #    maxValX = numpy.max(outImgX)
                    #    outImgX = (outImgX - minValX) / (maxValX - minValX)
                    #    outImgX = outImgX.astype(numpy.float32)
                    #    minValY = numpy.min(outImgY)
                    #    maxValY = numpy.max(outImgY)
                    #    outImgY = (outImgY - minValY) / (maxValY - minValY)
                    #    outImgY = outImgY.astype(numpy.float32)
                    #elif NORM_DATA_MODE == 1:
                    #    for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #        minValX = numpy.min(outImgX[:, :, channelIdx])
                    #        maxValX = numpy.max(outImgX[:, :, channelIdx])
                    #        outImgX[:, :, channelIdx] = (outImgX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    for channelIdx in itertools.islice(itertools.count(), 0, self.output_channels):
                    #        minValY = numpy.min(outImgY[:, :, channelIdx])
                    #        maxValY = numpy.max(outImgY[:, :, channelIdx])
                    #        outImgY[:, :, channelIdx] = (outImgY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #    outImgX = outImgX.astype(numpy.float32)
                    #    outImgY = outImgY.astype(numpy.float32)
                    #elif NORM_DATA_MODE == 2:
                    #    # ==== X-NORM ==== #
                    #    if self.x_type == TYPES['XRAY']:
                    #        outImgX = numpy.clip(numpy.divide(outImgX, self.flatField_input), 0.0, 1.0)
                    #    elif self.x_type == TYPES['SCATTER']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #            #minValX = numpy.min(self.flatField_input[:, :, channelIdx])
                    #            #maxValX = numpy.max(self.flatField_input[:, :, channelIdx])
                    #            #outImgX[:, :, channelIdx] = (outImgX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #            minValX = 0
                    #            maxValX = numpy.max(numpy.abs(outImgX[:, :, channelIdx]))
                    #            outImgX[:, :, channelIdx] = (outImgX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    elif self.x_type == TYPES['CT']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    #            minValX = numpy.min(outImgX[:, :, channelIdx])
                    #            maxValX = numpy.max(outImgX[:, :, channelIdx])
                    #            outImgX[:, :, channelIdx] = (outImgX[:, :, channelIdx] - minValX) / (maxValX - minValX)
                    #    # ==== Y-NORM ==== #
                    #    if self.y_type == TYPES['XRAY']:
                    #        outImgY = numpy.clip(numpy.divide(outImgY, self.flatField_output), 0.0, 1.0)
                    #    elif self.y_type == TYPES['SCATTER']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.output_channels):
                    #            #minValY = numpy.min(self.flatField_output[:, :, channelIdx])
                    #            #maxValY = numpy.max(self.flatField_output[:, :, channelIdx])
                    #            #outImgY[:, :, channelIdx] = (outImgY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #            minValY = 0
                    #            maxValY = numpy.max(numpy.abs(outImgY[:, :, channelIdx]))
                    #            outImgY[:, :, channelIdx] = (outImgY[:, :, channelIdx] - minValY) / (maxValY - minValY)
                    #    elif self.y_type == TYPES['CT']:
                    #        for channelIdx in itertools.islice(itertools.count(), 0, self.output_channels):
                    #            minValY = numpy.min(outImgY[:, :, channelIdx])
                    #            maxValY = numpy.max(outImgY[:, :, channelIdx])
                    #            outImgY[:, :, channelIdx] = (outImgY[:, :, channelIdx] - minValY) / (maxValY - minValY)

                    outImgX = outImgX.astype(numpy.float32)
                    outImgY = outImgY.astype(numpy.float32)

            """
            Data augmentation
            """
            input_target_size = self.target_size + (self.input_channels, )
            output_target_size = self.target_size + (self.output_channels, )
            if self.useZoom:
                outImgX = ndimage.zoom(outImgX, [self.zoomFactor, self.zoomFactor, 1], order=3, mode='reflect')
                outImgX = outImgX[self.im_bounds[0]:self.im_bounds[1], self.im_bounds[2]:self.im_bounds[3],
                          0:self.input_channels]
                outImgY = ndimage.zoom(outImgY, [self.zoomFactor, self.zoomFactor, 1], order=3, mode='constant')
                outImgY = outImgY[self.im_bounds[0]:self.im_bounds[1], self.im_bounds[2]:self.im_bounds[3],
                          0:self.output_channels]
            if self.useResize:
                outImgX = transform.resize(outImgX, input_target_size, order=3, mode='reflect')
                outImgY = transform.resize(outImgY, output_target_size, order=3, mode='constant')
            if self.useCrop:
                outImgX = outImgX[self.im_bounds[0]:self.im_bounds[1], self.im_bounds[2]:self.im_bounds[3],
                          0:self.input_channels]
                outImgY = outImgY[self.im_bounds[0]:self.im_bounds[1], self.im_bounds[2]:self.im_bounds[3],
                          0:self.output_channels]
            if self.useFlipping:
                mode = numpy.random.randint(0, 4)
                if mode == 0:  # no modification
                    pass
                if mode == 1:
                    outImgX = numpy.fliplr(outImgX)
                    outImgY = numpy.fliplr(outImgY)
                if mode == 2:
                    outImgX = numpy.flipud(outImgX)
                    outImgY = numpy.flipud(outImgY)
                if mode == 3:
                    outImgX = numpy.fliplr(outImgX)
                    outImgX = numpy.flipud(outImgX)
                    outImgY = numpy.fliplr(outImgY)
                    outImgY = numpy.flipud(outImgY)
            if self.useAWGN: # only applies to input data
                for channelIdx in itertools.islice(itertools.count(), 0, self.input_channels):
                    rectMin = numpy.min(outImgX[:,:,channelIdx])
                    rectMax = numpy.max(outImgX[:,:,channelIdx])
                    outImgX[:,:,channelIdx] = util.random_noise(outImgX[:,:,channelIdx], mode='gaussian', mean=self.MECTnoise_mu[channelIdx]*0.1, var=(self.MECTnoise_sigma[channelIdx]*0.1*self.MECTnoise_sigma[channelIdx]*0.1))
                    outImgX[:,:,channelIdx] = numpy.clip(outImgX[:,:,channelIdx], rectMin, rectMax)
            if self.useMedian:
                mSize = self.medianSize[numpy.random.randint(0,len(self.medianSize))]
                if mSize > 0:
                    outImgX = ndimage.median_filter(outImgX, (mSize, mSize, 1), mode='constant', cval=1.0)
                    # should the output perhaps always be median-filtered ?
                    #outImgY = ndimage.median_filter(outImgY, (mSize, mSize, 1), mode='constant', cval=1.0)
            if self.y_type==TYPES["XRAY"]:
                #outImgY = ndimage.median_filter(outImgY, (3, 3, 1), mode='constant', cval=1.0)
                outImgY = ndimage.median_filter(outImgY, (3, 3, 1), mode='reflect', cval=1.0)
            if self.useGaussian:
                # here, it's perhaps incorrect to also smoothen the output;
                # rationale: even an overly smooth image should result is sharp outputs
                sigma = numpy.random.uniform(low=self.gaussianRange[0], high=self.gaussianRange[1])
                outImgX = ndimage.gaussian_filter(outImgX, (sigma, sigma, 0))
                #outImgY = ndimage.gaussian_filter(outImgY, (sigma, sigma, 0))
            """
            Store data if requested
            """
            if self.save_to_dir != None:
                sXImg = array_to_img(outImgX[:, :, 0])
                # save_img(os.path.join(self.save_to_dir,fname_in+"."+self.save_format),sXimg)
                sXImg.save(os.path.join(self.save_to_dir, fname_in + "." + self.save_format))
                sYImg = array_to_img(outImgY[:, :, 0])
                # save_img(os.path.join(self.save_to_dir,fname_out+"."+self.save_format), sYImg)
                sYImg.save(os.path.join(self.save_to_dir, fname_out + "." + self.save_format))

            batchX[j] = outImgX
            batchY[j] = outImgY
            if self.useCache:
                self._lock_.acquire()
                self.cacheUsed_counter.value += 1
                if int(self.cacheUsed_counter.value) >= self.cache_period:
                    self.renew_cache.value = True
                self._lock_.release()

        return batchX, batchY

    def on_epoch_end(self):
        self.__initCache_open_()