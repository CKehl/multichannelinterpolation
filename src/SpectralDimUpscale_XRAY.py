"""
Created on Oct 2 2018

@author: chke (Christian Kehl)
"""

from __future__ import print_function

from keras.models import Input, Model
from keras.layers import Dense, SeparableConv2D, Conv2D, Concatenate, MaxPooling2D, Add
from keras.layers import UpSampling2D, Dropout
from keras.constraints import non_neg

class SpectralDimUpscale(object):
    """A simple multi-channel autoencoder in Keras"""

    def __init__(self):
        super(SpectralDimUpscale, self).__init__()
        self._activation = 'relu'
        self._dropout = True
        self._kernel_size = 3

        self.dataShape = None
        self.input = None  # Holder for Model input
        self.output = None  # Holder for Model output

    def begin(self, input_shape, output_shape):
        """
        Input here is an image of the forward scatter intensity
        of the measured data (c_0=1); In imaging terms, that's a 1-channel 2D image.
        """
        self.input_shape = input_shape
        self.output_shape = output_shape
        print("input shape {}".format(self.input_shape))
        self.input = Input(shape=self.input_shape)

    def finalize(self):
        """
        The expected output data is the scatter prediction; in train-test scenarios,
        this is the Monte Carlo simulation of the scattering.
        The output variable holds the network to achieve that.
        The function returns the model itself.
        """
        if self.input is None:
            raise RuntimeError("Missing an input. Use begin().")
        if self.output is None:
            raise RuntimeError("Missing an output. Use buildNetwork().")
        return Model(inputs=self.input, outputs=self.output)

    def buildNetwork(self):
        """
        This function builds the network exactly as-is from the paper of Maier et al.
        """
        print("Created input layer with shape {}".format(self.input.shape))
        inChannels = self.input_shape[2]
        numChannels = self.output_shape[2]
        input_conv = SeparableConv2D(int(inChannels), (self._kernel_size, self._kernel_size), depth_multiplier=2,
                                     activation=self._activation, padding='same', input_shape=self.input_shape,
                                     data_format="channels_last")(self.input)
        #dropout1_l1 = Dropout(0.2)(input_conv)
        conv1a_l2 = Conv2D(int(numChannels / 32), (self._kernel_size, self._kernel_size), activation=self._activation,padding='same')(input_conv)
        dropout1_l2 = Dropout(0.2)(conv1a_l2)
        conv1b_l2 = Conv2D(int(numChannels / 32), (self._kernel_size, self._kernel_size), activation=self._activation,padding='same')(dropout1_l2)
        conv1c_l2 = Conv2D(int(numChannels / 32), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv1b_l2)
        print("Created conv_1 layer 2 (channel upscaling) with shape {}".format(conv1c_l2.shape))
        conv2a_l2 = Conv2D(int(numChannels / 16), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv1c_l2)
        dropout2_l2 = Dropout(0.2)(conv2a_l2)
        conv2b_l2 = Conv2D(int(numChannels / 16), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout2_l2)
        conv2c_l2 = Conv2D(int(numChannels / 16), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv2b_l2)
        conv2d_l2 = Conv2D(int(numChannels / 16), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv2c_l2)
        print("Created conv_2 layer 2 (channel reduction) with shape {}".format(conv2d_l2.shape))
        conv3a_l2 = Conv2D(int(numChannels / 8), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv2d_l2)
        dropout3_l2 = Dropout(0.2)(conv3a_l2)
        conv3b_l2 = Conv2D(int(numChannels / 8), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout3_l2)
        conv3c_l2 = Conv2D(int(numChannels / 8), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv3b_l2)
        conv3d_l2 = Conv2D(int(numChannels / 8), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv3c_l2)
        print("Created conv_3 layer 2 (channel reduction) with shape {}".format(conv3d_l2.shape))
        conv4a_l2 = Conv2D(int(numChannels / 4), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv3d_l2)
        dropout4_l2 = Dropout(0.2)(conv4a_l2)
        conv4b_l2 = Conv2D(int(numChannels / 4), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout4_l2)
        conv4c_l2 = Conv2D(int(numChannels / 4), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv4b_l2)
        conv4d_l2 = Conv2D(int(numChannels / 4), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv4c_l2)
        print("Created conv_4 layer 2 (channel reduction) with shape {}".format(conv4d_l2.shape))
        conv5a_l2 = Conv2D(int(numChannels / 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv4d_l2)
        dropout5_l2 = Dropout(0.2)(conv5a_l2)
        conv5b_l2 = Conv2D(int(numChannels / 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout5_l2)
        conv5c_l2 = Conv2D(int(numChannels / 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv5b_l2)
        conv5d_l2 = Conv2D(int(numChannels / 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv5c_l2)
        print("Created conv_5 layer 2 (channel reduction) with shape {}".format(conv5d_l2.shape))
        conv6a_l2 = Conv2D(int(numChannels), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv5d_l2)
        dropout6_l2 = Dropout(0.2)(conv6a_l2)
        conv6b_l2 = Conv2D(int(numChannels), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dropout6_l2)
        conv6c_l2 = Conv2D(int(numChannels), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv6b_l2)
        conv6d_l2 = Conv2D(int(numChannels), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv6c_l2)
        print("Created conv_6 layer 2 (channel reduction) with shape {}".format(conv6d_l2.shape))

        #conv1_l3 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv6_l2)
        #dropout1_l3 = Dropout(0.1)(conv1_l3)
        #dense_1 = Dense(int(numChannels * 2), activation=self._activation)(dropout1_l3)
        #print("Created dense_1 layer 3 (feature learning) with shape {}".format(dense_1.shape))
        #conv2_l3 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation,padding='same')(dense_1)
        #dropout2_l3 = Dropout(0.1)(conv2_l3)
        #dense_2 = Dense(int(numChannels * 2), activation=self._activation)(dropout2_l3)
        #print("Created dense_2 layer 3 (feature learning) with shape {}".format(dense_2.shape))
        #conv3_l3 = Conv2D(int(numChannels * 2), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(dense_2)
        #dropout3_l3 = Dropout(0.1)(conv3_l3)
        #dense_3 = Dense(int(numChannels * 2), activation=self._activation)(dropout3_l3)
        #print("Created dense_3 layer 3 (feature learning) with shape {}".format(dense_3.shape))

        #possibly remove this - feature upsampling here is anyway a bit of a struggle
        #conv1_l4 = Conv2D(int(numChannels), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv6c_l2)
        ##dropout1_l4 = Dropout(0.2)(conv1_l4)
        #print("Created conv_1 layer 4 (feature learning) with shape {}".format(conv1_l4.shape))
        #conv2_l4 = Conv2D(int(numChannels), (self._kernel_size, self._kernel_size), activation=self._activation, padding='same')(conv1_l4)
        ##dropout2_l4 = Dropout(0.2)(conv2_l4)
        #print("Created conv_2 layer 4 (feature learning) with shape {}".format(conv2_l4.shape))

        # can be safely removed - compare xray_1 and xray_2
        #upDense = Dense(int(numChannels), activation=self._activation)(conv2_l4)
        #print("Created mid_dense layer 3 (feature learning) with shape {}".format(upDense.shape))

        self.output = SeparableConv2D(int(numChannels), (self._kernel_size, self._kernel_size), depth_multiplier=1,
                                      activation=self._activation, padding='same', data_format="channels_last")(conv6c_l2)
        print("Created output layer with shape {}".format(self.output.shape))




