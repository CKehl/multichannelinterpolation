#!python3
"""
Modified UNet training for scatter estimation 

Created on Jul 26, 2018

@author: christian (Christian Kehl)
@mail: chke@dtu.dk
"""

from __future__ import print_function
# from unetwork import UNetFactory
from SimpleAutoencoder import SimpleAutoencoder
# from UNet2D_Maier2018 import UNet2D_Maier2018
from MECTsequencer import MECTsequencer_inMem

from argparse import ArgumentParser
import os
from scipy import io
import h5py
import pickle
import keras
import numpy
from keras.preprocessing.image import ImageDataGenerator
from callbacks import ModelCheckpoint
import threading
import multiprocessing
import glob
import itertools
import re

"""
General parameters for learning
"""
input_indicator = "*Y*"
epochs = 20
keep_period = 1
# image_shape = (256,256,1)       # needs to fit with the actual data dim
# image_shape = (256,256,32)       # needs to fit with the actual data dim
image_shape = (150, 150, 32)  # needs to fit with the actual data dim
# image_shape = (437,437,1)       # needs to fit with the actual data dim
# image_shape = (437,437,32)       # needs to fit with the actual data dim
# targetImageShape = (256,256,1)  # free to choose for resizing
targetImageShape = (256, 256, 32)  # free to choose for resizing
scaleFactor = min(image_shape[0] / targetImageShape[0], image_shape[1] / targetImageShape[1])
# epoch_steps=16384
# epoch_steps=8192
epoch_steps = 4096
batchSize = 16
SCALE_FACTOR = 1.706666667
# cache_reuse_period=768
cache_reuse_period = 384
train_size = 4096
validation_size = 16


def loadHDF5(filename):
    """Loading data, reshaping and normalizing.

    Our training data is precomputed and stored on disk in a pickle file. The
    data is stored as a list of black and white images, i.e. a 3D array of
    integers from 0 to 255. We add the color dimensions, even though the length
    is one, and normalize the values between 0 and 1.
    """
    (pname, extName) = os.path.splitext(filename)
    if "h5" in extName:
        #print("Using HDF5 interface ...")
        file = h5py.File(filename,'r')
        data = numpy.array(file['data']['value'])
        file.close()
        return data
    else:
        pass
    return None


if __name__ == '__main__':
    description = ("simple auto-encoder for spectral up/downscaling.")
    option_parser = ArgumentParser(description=description)

    r_help = ("path to data folder with results (i.e. parent of output- and " +
              "model folder)")
    option_parser.add_argument("-r", "--resultpath",
                               action="store", dest="resultpath",
                               default="../data", help=r_help)
    m_help = ("name of the model file")
    option_parser.add_argument("-m", "--modelname",
                               action="store", dest="modelname",
                               default="reCT_epoch_{epoch:04d}", help=m_help)

    td_help = "training data directory"
    option_parser.add_argument("-T", "--train-dir",
                               action="store", dest="train-dir",
                               default="../data/train", help=td_help)
    vd_help = "validation data dir"
    option_parser.add_argument("-V", "--valid-dir",
                               action="store", dest="valid-dir",
                               default="../data/train", help=vd_help)
    ff_help = "flat field file path"
    option_parser.add_argument("-F", "--flatfield-path",
                               action="store", dest="flatfield-path",
                               default="../data/train/flatfield.h5", help=ff_help)
    aug_help = ("augment input data")
    option_parser.add_argument("--augment-input", action="store_true",
                               dest="augment", help=aug_help)

    # c_help = ("name of the continuing, previous model")
    # option_parser.add_argument("-c", "--continue_model", action="store", dest="continue_mfile", default="", help=c_help)

    options = option_parser.parse_args()
    arg_dict = vars(options)

    training_directory = os.path.join(arg_dict["train-dir"])
    validation_directory = os.path.join(arg_dict["valid-dir"])
    resultpath = arg_dict["resultpath"]

    if not os.path.exists(os.path.join(resultpath, "output")):
        os.makedirs(os.path.join(resultpath, "output"))
    if not os.path.exists(os.path.join(resultpath, "models")):
        os.makedirs(os.path.join(resultpath, "models"))

    flatfield_path = None
    flatfield = None
    if (arg_dict["flatfield-path"] != None):
        flatfield_path = arg_dict["flatfield-path"]
        f = h5py.File(flatFieldFilePath, 'r')
        flatField = numpy.array(f['data']['value'])  # f['data0']
        f.close()

    modelfilename = arg_dict["modelname"]
    mhistfile = os.path.join(resultpath, "output", modelfilename + "_Thist.pkl")
    weightfile = os.path.join(resultpath, "models", modelfilename + "_weights.h5")
    modelfile = os.path.join(resultpath, "models", modelfilename + "_model.h5")

    """
    Data processing - loading, sampling, modification, etc.
    """
    print("Start data processing ...")
    inputFileArray_train = []
    # scatterMapArray_train = []
    # observeFileArray_train = []
    inputFileArray_validation = []
    # scatterMapArray_validation = []
    # observeFileArray_validation = []

    for name in glob.glob(os.path.join(training_directory, '*X*.h5')):
        inputFileArray_train.append(name)
    # for name in glob.glob(os.path.join(training_directory, '*Y*.h5')):
    #    scatterMapArray_train.append(name)
    # for name in glob.glob(os.path.join(training_directory,'*Z*.h5')):
    #    observeFileArray_train.append(name)
    for name in glob.glob(os.path.join(validation_directory, '*X*.h5')):
        inputFileArray_validation.append(name)
    # for name in glob.glob(os.path.join(validation_directory, '*Y*.h5')):
    #    scatterMapArray_validation.append(name)
    # for name in glob.glob(os.path.join(validation_directory,'*Z*.h5')):
    #    observeFileArray_validation.append(name)

    digits = re.compile(r'(\d+)')
    def tokenize(filename):
        return tuple(int(token) if match else token for token, match in
                     ((fragment, digits.search(fragment)) for fragment in digits.split(filename)))


    inputFileArray_train.sort(key=tokenize)
    # scatterMapArray_train.sort(key=tokenize)
    # observeFileArray_train.sort(key=tokenize)
    inputFileArray_validation.sort(key=tokenize)
    # scatterMapArray_validation.sort(key=tokenize)
    # observeFileArray_validation.sort(key=tokenize)
    len_train = min(len(inputFileArray_train), train_size)
    len_validation = min(len(inputFileArray_validation), validation_size)
    nTrainImages = len_train
    nValidImages = len_validation

    if len_train == 0:
        print("No training data found. Exiting.")
        exit()
    if len_validation == 0:
        print("No validation data found. Exiting.")
        exit()
    dumpDataFile = h5py.File(inputFileArray_train[0], 'r')
    # dumpData = numpy.array(dumpDataFile['dataX'], order='F').transpose()
    dumpData = numpy.array(dumpDataFile['data']['value'], dtype=numpy.float32)
    dumpDataFile.close()

    if len(dumpData.shape) < 3:
        inDataTrain = numpy.zeros((len_train, dumpData.shape[0], dumpData.shape[1], 1), dtype=numpy.float32)
    #    outScatterTrain = numpy.zeros((len_train, dumpData.shape[0], dumpData.shape[1], 1), dtype=numpy.float32)
    #    outDataTrain = numpy.zeros((len_train, dumpData.shape[0], dumpData.shape[1], 1), dtype=numpy.float32)
        for i in itertools.islice(itertools.count(), 0, len_train):
            inDataTrain[i, :, :, 0] = loadHDF5(inputFileArray_train[i])
    #        outScatterTrain[i, :, :, 0] = loadHDF5(scatterMapArray_train[i])
    #        outDataTrain[i,:,:,0] = loadHDF5(observeFileArray_train[i])
        inDataValid = numpy.zeros((len_validation, dumpData.shape[0], dumpData.shape[1], 1), dtype=numpy.float32)
    #    outScatterValid = numpy.zeros((len_validation, dumpData.shape[0], dumpData.shape[1], 1), dtype=numpy.float32)
    #    outDataValid = numpy.zeros((len_validation, dumpData.shape[0], dumpData.shape[1], 1), dtype=numpy.float32)
        for i in itertools.islice(itertools.count(), 0, len_validation):
            inDataValid[i, :, :, 0] = loadHDF5(inputFileArray_validation[i])
    #        outScatterValid[i, :, :, 0] = loadHDF5(scatterMapArray_validation[i])
    #        outDataValid[i,:,:,0] = loadHDF5(observeFileArray_validation[i])
    elif len(dumpData.shape) < 4:
        inDataTrain = numpy.zeros((len_train, dumpData.shape[0], dumpData.shape[1], dumpData.shape[2]), dtype=numpy.float32)
    #    outScatterTrain = numpy.zeros((len_train, dumpData.shape[0], dumpData.shape[1], dumpData.shape[2]), dtype=numpy.float32)
    #    outDataTrain = numpy.zeros((len_train, dumpData.shape[0], dumpData.shape[1], dumpData.shape[2]), dtype=numpy.float32)
        for i in itertools.islice(itertools.count(), 0, len_train):
            inDataTrain[i] = loadHDF5(inputFileArray_train[i])
    #        outScatterTrain[i] = loadHDF5(scatterMapArray_train[i])
    #        outDataTrain[i] = loadHDF5(observeFileArray_train[i])
        inDataValid = numpy.zeros((len_validation, dumpData.shape[0], dumpData.shape[1], dumpData.shape[2]), dtype=numpy.float32)
    #    outScatterValid = numpy.zeros((len_validation, dumpData.shape[0], dumpData.shape[1], dumpData.shape[2]), dtype=numpy.float32)
    #    outDataTrain = numpy.zeros((len_validation, dumpData.shape[0], dumpData.shape[1], dumpData.shape[2]), dtype=numpy.float32)
        for i in itertools.islice(itertools.count(), 0, len_validation):
            inDataValid[i] = loadHDF5(inputFileArray_validation[i])
    #        outScatterValid[i] = loadHDF5(scatterMapArray_validation[i])
    #        outDataTrain[i] = loadHDF5(observeFileArray_validation[i])
    else:
        inDataTrain = loadHDF5(inputFileArray_train[i])
    #    outScatterTrain = loadHDF5(scatterMapArray_train[i])
    #    outDataTrain = loadHDF5(observeFileArray_train[i])
        inDataValid = loadHDF5(inputFileArray_validation[i])
    #    outScatterValid = loadHDF5(scatterMapArray_validation[i])
    #    outDataTrain = loadHDF5(observeFileArray_validation[i])
    print("Finished data processing")

    latest_epoch, latest_model = 0, None
    ModelCheckpoint.remove_all_checkpoints(modelfile, weightfile)

    """
    Our UNet model is defined in the module unetwork stored locally. Using a
    factory class we can easyly generate a custom model using very few steps.

    unet_factory.dropout = None
    unet_factory.convolution_kernel_size = 3
    unet_factory.batch_normalization = False
    unet_factory.begin(image_shape=image_shape)
    unet_factory.generateLevels(init_filter_count=32, recursive_depth=4)
    model = unet_factory.finalize(final_filter_count=1)

    Here, using the specific network of Maier et al. 2018 on Deep Scatter Estimation (DSE)
    """
    nnet = SimpleAutoencoder()
    # nnet = UNet2D_Maier2018()
    nnet.begin(image_shape=targetImageShape)
    nnet.buildNetwork(targetImageShape)
    model = nnet.finalize()

    """
    Adding an optimizer (optional)---can be used to optimize gradient decent.
    We employ the rmsprop: divide the gradient by a running average of its
    recent magnitude.
    These are, for the moment, arbitrarily chosen by the original author. The
    work would benifit from a thorough walk through these choices.
    """
    # opt = keras.optimizers.rmsprop(lr=0.0001,  # Learning rate (lr)
    #                               decay=1e-6)  # lr decay over each update.
    opt = keras.optimizers.Adam(lr=0.0001, decay=1e-6, amsgrad=False)
    model.compile(loss='mean_absolute_error',  # standard cost function
                  optimizer=opt,
                  metrics=['mse'])  # Mean Absolute Error metrics

    """
    Data augmentation setup
    """
    augment = arg_dict["augment"]

    datagen = MECTsequencer_inMem(inDataTrain, inDataTrain, image_shape, batchSize,
                                  targetSize=(targetImageShape[0], targetImageShape[1]),
                                  useResize=True, useZoom=False, useFlipping=True, flatField=flatfield)
    validgen = MECTsequencer_inMem(inDataValid, inDataValid, image_shape, batchSize,
                                   targetSize=(targetImageShape[0], targetImageShape[1]),
                                   useResize=True, useZoom=False, useFlipping=False, flatField=flatfield)

    callbacks = []
    callbacks.append(ModelCheckpoint(modelpath=modelfile,
                                     weightspath=weightfile,
                                     period=1,
                                     auto_remove_model=True,
                                     auto_remove_weights=True,
                                     keep_period=keep_period))

    import subprocess

    subprocess.Popen(
        "timeout 200 nvidia-smi --query-gpu=utilization.gpu,utilization.memory --format=csv -l 1 \ sed s/%//g > ./GPU_stats.log",
        shell=True)

    model_fitting_history = model.fit_generator(datagen,
                                                steps_per_epoch=int(epoch_steps / batchSize),
                                                # validation_data=(inDataValid, inDataValid),
                                                validation_data=validgen,
                                                validation_steps=int(nValidImages / batchSize),
                                                epochs=epochs,
                                                use_multiprocessing=True,
                                                workers=12,
                                                callbacks=callbacks)

    with open(mhistfile.format(epoch=epochs), 'wb') as file:
        pickle.dump(model_fitting_history.history, file)