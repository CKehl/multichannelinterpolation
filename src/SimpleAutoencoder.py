from __future__ import print_function

from keras.models import Input, Model
from keras.layers import Dense, Conv2D, Concatenate, MaxPooling2D, Add
from keras.layers import UpSampling2D, Dropout, BatchNormalization

class SimpleAutoencoder(object):
    """A simple multi-channel autoencoder in Keras"""

    def __init__(self):
        super(SimpleAutoencoder, self).__init__()
        self._activation = 'relu'
        self._dropout = True

        self.dataShape = None
        self.input = None  # Holder for Model input
        self.output = None  # Holder for Model output

    def begin(self, image_shape):
        """
        Input here is an image of the forward scatter intensity
        of the measured data (c_0=1); In imaging terms, that's a 1-channel 2D image.
        """
        self.dataShape = image_shape
        print("input shape {}".format(image_shape))
        self.input = Input(shape=image_shape)

    def finalize(self):
        """
        The expected output data is the scatter prediction; in train-test scenarios,
        this is the Monte Carlo simulation of the scattering.
        The output variable holds the network to achieve that.
        The function returns the model itself.
        """
        if self.input is None:
            raise RuntimeError("Missing an input. Use begin().")
        if self.output is None:
            raise RuntimeError("Missing an output. Use buildNetwork().")
        return Model(inputs=self.input, outputs=self.output)

    def buildNetwork(self, inShapePerImage=None):
        """
        This function builds the network exactly as-is from the paper of Maier et al.
        """
        print("Created input layer with shape {}".format(self.input.shape))
        numChannels = self.dataShape[2]

        # Encoder
        hidden_1 = Dense(16, activation='relu')(self.input)
        print("Created FC layer 1 with shape {}".format(hidden_1.shape))
        h = Dense(1, activation='relu')(hidden_1)
        print("Created FC layer 2 with shape {}".format(h.shape))

        # Decoder
        hidden_2 = Dense(16, activation='relu')(h)
        print("Created decode FC layer 1 with shape {}".format(hidden_2.shape))
        #print(self.input.shape[3])
        self.output = Dense(int(self.input.shape[3]), activation='sigmoid')(hidden_2)
        # Encoder
        #conv1_1 = Conv2D(8*numChannels, (3, 3), activation=self._activation, padding='same', input_shape=inShapePerImage, data_format="channels_last")(self.input)
        #self.output = Conv2D(numChannels, (3, 3), activation='sigmoid', padding='same', data_format="channels_last")(up3)
        print("Created output layer with shape {}".format(self.output.shape))