import numpy
#import threading
import multiprocessing
import ctypes
from scipy import linalg
from scipy import ndimage
from keras.preprocessing.image import load_img, img_to_array, array_to_img#, save_img
from keras.utils import Sequence
import glob
#from PIL import Image
from skimage import util
from skimage import transform
import itertools
import os
import re
import h5py


WORKERS = 12
CACHE_SIZE = 32
dims = (256,256,1)
NORM_DATA_MODE = 0  # 0 - per image over all channels; 1 - per image per channel; 2 - flat-field norm

class MECTsequencer(Sequence):
    
    def __init__(self, image_size=(128, 128,1), batch_size=1, useAWGN = False, gauss_mu=0.5, gauss_stdDev=0.1, useRotation=False, rotationRange=(0,360), targetSize=(128,128), useResize=False, useZoom=False, zoomFactor=1.0, useFlipping=False, useNormData=False, cache_period=512, save_to_dir=None, save_format="png", threadLockVar=None, useCache=False):
        self.batch_size = batch_size
        self.image_size = image_size
        #self.image_path = image_path
        #self.augment_flow = augment_flow
        #self.k = (self.kernel_size-1)//2
        self.dtype = numpy.float32
        self.useAWGH = useAWGN
        self.gauss_mu = gauss_mu
        self.gauss_stdDev = gauss_stdDev
        self.rotationRange = rotationRange
        self.targetSize=targetSize
        self.useRotation = useRotation
        self.useResize = useResize
        self.zoomFactor = zoomFactor
        self.useZoom = useZoom
        self.useFlipping = useFlipping
        self.useNormData = useNormData
        self.numImages = 0
        dims = targetSize
        #self.targetSize = (self.targetSize[0], self.targetSize[1], self.image_size[2])
        #========================================#
        #== zoom-related image information ==#
        #========================================#
        #self.im_center = numpy.array([(self.image_size[0]-1)/2, (self.image_size[1]-1)/2], dtype=numpy.int32)
        #self.im_shift = numpy.array([(self.targetSize[0]-1)/2, (self.targetSize[1]-1)/2], dtype=numpy.int32)
        self.im_center = numpy.array([int(self.image_size[0]*self.zoomFactor-1)/2, int(self.image_size[1]*self.zoomFactor-1)/2], dtype=numpy.int32)
        self.im_shift = numpy.array([(self.image_size[0]-1)/2, (self.image_size[1]-1)/2], dtype=numpy.int32)
        left = self.im_center[0]-self.im_shift[0]
        #right = left+self.targetSize[0]
        right = left+self.image_size[0]
        top = self.im_center[1]-self.im_shift[1]
        #bottom = top+self.targetSize[1]
        bottom = top+self.image_size[1]
        self.im_bounds = (left,right,top,bottom)
        #===================================#
        #== directory-related information ==#
        #===================================#
        self.inputFileArray = []
        self.scatterMapArray = []
        self.observeFileArray = []
        self.save_to_dir=save_to_dir
        self.save_format=save_format
        #==================================#
        #== flat-field related variables ==#
        #==================================#
        self.flatField_input = None
        self.flatField_scatter = None
        self.flatField_output = None
        self.useFlatFieldNorm = False
        #===============================#
        #== caching-related variables ==#
        #===============================#
        self.useCache = useCache
        self.cache_size = CACHE_SIZE
        self.cache_period = cache_period
        self.cacheX = numpy.zeros(1,dtype=numpy.float32)
        self.cacheY = numpy.zeros(1,dtype=numpy.float32)
        #self.renew_cache = False
        self.renew_cache = multiprocessing.Value(ctypes.c_bool,False)
        self.cacheUsed_counter = multiprocessing.Value('i',0)
        self.cacheRenewed_counter = multiprocessing.Value('i',0)
        self._lock_ = threadLockVar
        self._memlock_ = multiprocessing.Lock()
        #self._refreshCondition_ = threading.Condition()
        #self._refreshBarrier_ = threading.Barrier(12)
        #self._refreshEvent_ = threading.Event()
        self._refreshEvent_ = multiprocessing.Event()
        #self._refreshBarrier_ = multiprocessing.Barrier(WORKERS)
        
    def prepareDirectFileInput(self, image_paths, flatFieldFilePath=None):
        for entry in image_paths:
            for name in glob.glob(os.path.join(entry,'*X*.h5')):
                self.inputFileArray.append(name)
            for name in glob.glob(os.path.join(entry,'*Y*.h5')):
                self.scatterMapArray.append(name)
            for name in glob.glob(os.path.join(entry,'*Z*.h5')):
                self.observeFileArray.append(name)
        digits = re.compile(r'(\d+)')
        def tokenize(filename):
            return tuple(int(token) if match else token for token, match in ((fragment, digits.search(fragment)) for fragment in digits.split(filename)))
        # Now you can sort your file names like so:
        self.inputFileArray.sort(key=tokenize)
        self.observeFileArray.sort(key=tokenize)
        self.scatterMapArray.sort(key=tokenize)
        self.numImages = len(self.inputFileArray)
        # prepare image sizes #
        f = h5py.File(self.inputFileArray[0], 'r')
        outImgX = numpy.array(f['data']['value'])
        f.close()
        if len(outImgX)<3:
            outImgX = outImgX.reshape(outImgX.shape + (1,))
        self.image_size =outImgX.shape
        # prepare caching #
        if self.useCache:
            self.cacheX = numpy.zeros((self.cache_size,self.image_size[0],self.image_size[1],self.image_size[2]),dtype=numpy.float32)
            self.cacheY = numpy.zeros((self.cache_size,self.image_size[0],self.image_size[1],self.image_size[2]),dtype=numpy.float32)
            #self.renew_cache = False
            self.renew_cache = multiprocessing.Value(ctypes.c_bool,False)
            self.cacheUsed_counter = multiprocessing.Value('i',0)
            self.__initCache_open_()
        # prepare flat-field normalization #
        if (flatFieldFilePath != None) and (len(flatFieldFilePath)>0):
            f = h5py.File(flatFieldFilePath, 'r')
            darray = numpy.array(f['data']['value']) #f['data0']
            f.close()
            self.flatField_input = darray
            self.flatField_output = darray
            # flat-field for scatter ???? #
            self.useFlatFieldNorm = True
            NORM_DATA_MODE = 2

    def _initCache_locked_(self):
        startId = 0
        loadData_flag = True
        #wait_flag = True

        with self._lock_:
            startId = self.cacheRenewed_counter.value
        #    if (startId == 0) and (self._refreshEvent_.is_set()):
        #        self._refreshEvent_.clear()
            if(startId>=self.cache_size):
                loadData_flag = False
            else:
                loadData_flag=True
                self.cacheRenewed_counter.value+=self.batch_size
        
        #with self._lock_:
        ##with self.renew_cache.get_lock():
        #    wait_flag = self.renew_cache.value;
            
        if loadData_flag == True:
            # ---------------- #
            # repopulate cache #
            # ---------------- #
            idxArray = numpy.random.randint(0, self.numImages, self.cache_size)
            for ii in itertools.islice(itertools.count(),startId,min([startId+self.batch_size, self.cache_size])):
                #imgIndex = numpy.random.randint(0, self.numImages-1)
                imgIndex = idxArray[ii]
                inName = self.inputFileArray[imgIndex]
                #outName = self.observeFileArray[imgIndex]
                outName = self.scatterMapArray[imgIndex]
                f = h5py.File(inName, 'r')
                imX = numpy.array(f['data']['value'])
                f.close()
                if len(imX)<3:
                    imX = imX.reshape(imX.shape + (1,))
                f = h5py.File(outName, 'r')
                imY = numpy.array(f['data']['value'])
                f.close()
                if len(imY)<3:
                    imY = imY.reshape(imY.shape + (1,))
                if imX.shape != imY.shape:
                    raise RuntimeError("Input- and Output sizes do not match.")

                #== Note: do data normalization here to reduce memory footprint ==#
                if self.useNormData:
                    if NORM_DATA_MODE==0:
                        minValX = numpy.min(imX)
                        maxValX = numpy.max(imX)
                        imX = (imX-minValX)/(maxValX-minValX)
                        imX = imX.astype(numpy.float32)
                        minValY = numpy.min(imY)
                        maxValY = numpy.max(imY)
                        imY = (imY-minValY)/(maxValY-minValY)
                        imY = imY.astype(numpy.float32)
                    elif NORM_DATA_MODE==1:
                        numChannels = self.image_size[2]
                        for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                            minValX = numpy.min(imX[:,:,channelIdx])
                            maxValX = numpy.max(imX[:,:,channelIdx])
                            imX[:,:,channelIdx] = (imX[:,:,channelIdx]-minValX)/(maxValX-minValX)
                            minValY = numpy.min(imY[:,:,channelIdx])
                            maxValY = numpy.max(imY[:,:,channelIdx])
                            imY[:,:,channelIdx] = (imY[:,:,channelIdx]-minValY)/(maxValY-minValY)
                        imX = imX.astype(numpy.float32)
                        imY = imY.astype(numpy.float32)
                    elif NORM_DATA_MODE==2:
                        imX = numpy.clip(numpy.divide(imX,self.flatField_input), 0.0, 1.0)
                        numChannels = self.image_size[2]
                        for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                            #minValY = numpy.min(imY[:,:,channelIdx])
                            #maxValY = numpy.max(imY[:,:,channelIdx])
                            minValY = numpy.min(self.flatField_input[:,:,channelIdx])
                            maxValY = numpy.max(self.flatField_input[:,:,channelIdx])
                            imY[:,:,channelIdx] = (imY[:,:,channelIdx]-minValY)/(maxValY-minValY)
                        imX = imX.astype(numpy.float32)
                        imY = imY.astype(numpy.float32)
                with self._memlock_:
                    self.cacheX[ii] = imX
                    self.cacheY[ii] = imY
        #    if (startId+self.batch_size)>=self.cache_size:
        #        self._refreshEvent_.set()
        #    else:
        #        try:
        #            self._refreshEvent_.wait()
        #        except Exception as e:
        #            pass
        #elif wait_flag == True:
        #    try:
        #        self._refreshEvent_.wait()
        #    except Exception as e:
        #        pass
        ##    pass
        else:
            return

        #self._refreshBarrier_.wait()
        #with self._lock_:
        #    if self._refreshEvent_.is_set():
        #        self._refreshEvent_.clear()
        #    self._refreshBarrier_.reset()

        with self._lock_:
            if self.cacheRenewed_counter.value >= self.cache_size:
                self.cacheRenewed_counter.value = 0

            if self.cacheUsed_counter.value >= self.cache_period:
                self.cacheUsed_counter.value=0

            if((startId+self.batch_size)>=self.cache_size):
                self.renew_cache.value=False
        return


    def __initCache_open_(self):
        if self.useCache:
            # ---------------- #
            # repopulate cache #
            # ---------------- #
            idxArray = numpy.random.randint(0, self.numImages, self.cache_size)
            for ii in itertools.islice(itertools.count(),0,self.cache_size):
                #imgIndex = numpy.random.randint(0, self.numImages-1)
                imgIndex = idxArray[ii]
                inName = self.inputFileArray[imgIndex]
                #outName = self.observeFileArray[imgIndex]
                outName = self.scatterMapArray[imgIndex]
                f = h5py.File(inName, 'r')
                outImgX = numpy.array(f['data']['value'])
                f.close()
                if len(outImgX)<3:
                    outImgX = outImgX.reshape(outImgX.shape + (1,))
                f = h5py.File(outName, 'r')
                outImgY = numpy.array(f['data']['value'])
                f.close()
                if len(outImgY)<3:
                    outImgY = outImgY.reshape(outImgY.shape + (1,))
                if outImgX.shape != outImgY.shape:
                    raise RuntimeError("Input- and Output sizes do not match.")
    
                #== Note: do data normalization here to reduce memory footprint ==#
                if self.useNormData:
                    if NORM_DATA_MODE==0:
                        minValX = numpy.min(outImgX)
                        maxValX = numpy.max(outImgX)
                        outImgX = (outImgX-minValX)/(maxValX-minValX)
                        outImgX = outImgX.astype(numpy.float32)
                        minValY = numpy.min(outImgY)
                        maxValY = numpy.max(outImgY)
                        outImgY = (outImgY-minValY)/(maxValY-minValY)
                        outImgY = outImgY.astype(numpy.float32)
                    elif NORM_DATA_MODE==1:
                        numChannels = self.image_size[2]
                        for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                            minValX = numpy.min(outImgX[:,:,channelIdx])
                            maxValX = numpy.max(outImgX[:,:,channelIdx])
                            outImgX[:,:,channelIdx] = (outImgX[:,:,channelIdx]-minValX)/(maxValX-minValX)
                            minValY = numpy.min(outImgY[:,:,channelIdx])
                            maxValY = numpy.max(outImgY[:,:,channelIdx])
                            outImgY[:,:,channelIdx] = (outImgY[:,:,channelIdx]-minValY)/(maxValY-minValY)
                        outImgX = outImgX.astype(numpy.float32)
                        outImgY = outImgY.astype(numpy.float32)
                    elif NORM_DATA_MODE==2:
                        outImgX = numpy.clip(numpy.divide(outImgX,self.flatField_input), 0.0, 1.0)
                        numChannels = self.image_size[2]
                        for channelIdx in itertools.islice(itertools.count(), 0, numChannels):
                            #minValY = numpy.min(imY[:,:,channelIdx])
                            #maxValY = numpy.max(imY[:,:,channelIdx])
                            minValY = numpy.min(self.flatField_input[:,:,channelIdx])
                            maxValY = numpy.max(self.flatField_input[:,:,channelIdx])
                            outImgY[:,:,channelIdx] = (outImgY[:,:,channelIdx]-minValY)/(maxValY-minValY)
                        outImgX = outImgX.astype(numpy.float32)
                        outImgY = outImgY.astype(numpy.float32)
                with self._memlock_:
                    self.cacheX[ii,:,:,:] = outImgX
                    self.cacheY[ii,:,:,:] = outImgY

    def __len__(self):
        return int(numpy.ceil(len(self.inputFileArray)/float(self.batch_size)))
    
    def __getitem__(self, idx):
        if self.useCache:
            flushCache = False
            with self._lock_:
                flushCache = self.renew_cache.value
            if flushCache == True:
                self._initCache_locked_()
        batchX = numpy.zeros((self.batch_size,self.image_size[0],self.image_size[1],self.image_size[2]),dtype=self.dtype)
        batchY = numpy.zeros((self.batch_size,self.image_size[0],self.image_size[1],self.image_size[2]),dtype=self.dtype)
        if self.useResize:
            batchX = numpy.zeros((self.batch_size,self.targetSize[0],self.targetSize[1],self.targetSize[2]),dtype=self.dtype)
            batchY = numpy.zeros((self.batch_size,self.targetSize[0],self.targetSize[1],self.targetSize[2]),dtype=self.dtype)

        idxArray = numpy.random.randint(0, self.cache_size, self.batch_size)
        for j in itertools.islice(itertools.count(),0,self.batch_size):
            outImgX = None
            outImgY = None
            if self.useCache:
                #imgIndex = numpy.random.randint(0, self.cache_size)
                imgIndex =idxArray[j]
                with self._memlock_:
                    outImgX = self.cacheX[imgIndex]
                    outImgY = self.cacheY[imgIndex]
            else:
                #imgIndex = min([(idx*self.batch_size)+j, self.numImages-1,len(self.inputFileArray)-1])
                imgIndex = ((idx*self.batch_size)+j) % (self.numImages-1)
                """
                Load data from disk
                """
                inName = self.inputFileArray[imgIndex]
                #outName = self.observeFileArray[imgIndex]
                outName = self.scatterMapArray[imgIndex]
                f = h5py.File(inName, 'r')
                outImgX = numpy.array(f['data']['value'])
                f.close()
                if len(outImgX)<3:
                    outImgX = outImgX.reshape(outImgX.shape + (1,))
                f = h5py.File(outName, 'r')
                outImgY = numpy.array(f['data']['value'])
                f.close()
                if len(outImgY)<3:
                    outImgY = outImgY.reshape(outImgY.shape + (1,))
                if outImgX.shape != outImgY.shape:
                    raise RuntimeError("Input- and Output sizes do not match.")
                #== Note: do data normalization here to reduce memory footprint ==#
                if self.useNormData:
                    minValX = numpy.min(outImgX)
                    maxValX = numpy.max(outImgX)
                    outImgX = (outImgX-minValX)/(maxValX-minValX)
                    outImgX = outImgX.astype(numpy.float32)
                    minValY = numpy.min(outImgY)
                    maxValY = numpy.max(outImgY)
                    outImgY = (outImgY-minValY)/(maxValY-minValY)
                    outImgY = outImgY.astype(numpy.float32)
                
            """
            Data augmentation
            """
            if self.useZoom:
                outImgX = ndimage.zoom(outImgX, [self.zoomFactor,self.zoomFactor,1], order=3, mode='reflect')
                outImgX = outImgX[self.im_bounds[0]:self.im_bounds[1],self.im_bounds[2]:self.im_bounds[3],0:self.image_size[2]]
                outImgY = ndimage.zoom(outImgY, [self.zoomFactor,self.zoomFactor,1], order=3, mode='constant')
                outImgY = outImgY[self.im_bounds[0]:self.im_bounds[1],self.im_bounds[2]:self.im_bounds[3],0:self.image_size[2]]
            if self.useRotation:
                outImgX = ndimage.rotate(outImgX, numpy.random.uniform(self.rotationRange[0], self.rotationRange[1]), axes=(1,0), order=3, mode='reflect')
                outImgX = numpy.clip(outImgX, 0.0, 1.0)
                outImgY = ndimage.rotate(outImgY, numpy.random.uniform(self.rotationRange[0], self.rotationRange[1]), axes=(1,0), order=3, mode='constant')
                outImgY = numpy.clip(outImgY, 0.0, 1.0)
            if self.useResize:
                ## outImgX = outImgX[self.im_bounds[0]:self.im_bounds[1],self.im_bounds[2]:self.im_bounds[3],0:self.targetSize[2]]
                ## outImgY = outImgY[self.im_bounds[0]:self.im_bounds[1],self.im_bounds[2]:self.im_bounds[3],0:self.targetSize[2]]
                #tmpX = numpy.array(outImgX)
                #tmpY = numpy.array(outImgY)
                #outImgX = numpy.zeros((self.targetSize[0],self.targetSize[1],self.targetSize[2]),dtype=self.dtype)
                #outImgY = numpy.zeros((self.targetSize[0],self.targetSize[1],self.targetSize[2]),dtype=self.dtype)
                #for eb in itertools.islice(itertools.count(), 0, self.image_size[2]):
                #    outImgX[:,:,eb] = transform.resize(tmpX[:,:,eb], self.targetSize, order=3)
                #    outImgY[:,:,eb] = transform.resize(tmpY[:,:,eb], self.targetSize, order=3)
                outImgX = transform.resize(outImgX, self.targetSize, order=3, mode='reflect')
                outImgY = transform.resize(outImgY, self.targetSize, order=3, mode='constant')
            if self.useAWGH: # only applies to input data
                outImgX = util.random_noise(outImgX, mode='gaussian', mean=self.gauss_mu, var=(self.gauss_stdDev*self.gauss_stdDev))
                outImgX = numpy.clip(outImgX, 0.0, 1.0)
            if self.useFlipping:
                mode = numpy.random.randint(0,4)
                if mode == 0:   # no modification
                    pass
                if mode == 1:
                    outImgX = numpy.fliplr(outImgX)
                    outImgY = numpy.fliplr(outImgY)
                    #outImgX = ndimage.geometric_transform(outImgX, flipSpectralX, mode='nearest')
                    #outImgY = ndimage.geometric_transform(outImgY, flipSpectralX, mode='nearest')
                if mode == 2:
                    outImgX = numpy.flipud(outImgX)
                    outImgY = numpy.flipud(outImgY)
                    #outImgX = ndimage.geometric_transform(outImgX, flipSpectralY, mode='nearest')
                    #outImgY = ndimage.geometric_transform(outImgY, flipSpectralY, mode='nearest')
                if mode == 3:
                    outImgX = numpy.fliplr(outImgX)
                    outImgX = numpy.flipud(outImgX)
                    outImgY = numpy.fliplr(outImgY)
                    outImgY = numpy.flipud(outImgY)
                    #outImgX = ndimage.geometric_transform(outImgX, flipSpectralXY, mode='nearest')
                    #outImgY = ndimage.geometric_transform(outImgY, flipSpectralXY, mode='nearest')
            """
            Store data if requested
            """
            if self.save_to_dir != None:
                sXImg = array_to_img(outImgX[:,:,0])
                #save_img(os.path.join(self.save_to_dir,fname_in+"."+self.save_format),sXimg)
                sXimg.save(os.path.join(self.save_to_dir,fname_in+"."+self.save_format))
                sYImg = array_to_img(outImgY[:,:,0])
                #save_img(os.path.join(self.save_to_dir,fname_out+"."+self.save_format), sYImg)
                sYimg.save(os.path.join(self.save_to_dir,fname_out+"."+self.save_format))
            batchX[j] = outImgX
            batchY[j] = outImgY
            if self.useCache:
                self._lock_.acquire()
                self.cacheUsed_counter.value+=1
                if int(self.cacheUsed_counter.value) >= self.cache_period:
                    self.renew_cache.value = True
                self._lock_.release()
        return batchX, batchY
    
    def on_epoch_end(self):
        self.__initCache_open_()

class MECTsequencer(Sequence):
    
    def __init__(self, images_in, images_out, image_size=(128, 128,1), batch_size=1, useAWGN = False, gauss_mu=0.5, gauss_stdDev=0.1, useRotation=False, rotationRange=(0,360), targetSize=(128,128), useZoom=False, zoomFactor=1.0, useFlipping=False, useNormData=False, save_to_dir=None, save_format="png"):
        self.batch_size = batch_size
        self.image_size = image_size
        #self.image_path = image_path
        #self.augment_flow = augment_flow
        #self.k = (self.kernel_size-1)//2
        self.dtype = numpy.float32
        self.useAWGH = useAWGN
        self.gauss_mu = gauss_mu
        self.gauss_stdDev = gauss_stdDev
        self.rotationRange = rotationRange
        self.targetSize=targetSize
        self.useRotation = useRotation
        self.useClip = useRotation
        self.zoomFactor = zoomFactor
        self.useZoom = useZoom
        self.useFlipping = useFlipping
        self.useNormData = useNormData
        dims = targetSize
        self.targetSize = (self.targetSize[0], self.targetSize[1], self.image_size[2])
        #========================================#
        #== clipping-related image information ==#
        #========================================#
        self.im_center = numpy.array([(self.image_size[0]-1)/2, (self.image_size[1]-1)/2], dtype=numpy.int32)
        self.im_size = numpy.array([(self.targetSize[0]-1)/2, (self.targetSize[1]-1)/2], dtype=numpy.int32)
        left = self.im_center[0]-self.im_size[0]
        right = left+self.targetSize[0]
        top = self.im_center[1]-self.im_size[1]
        bottom = top+self.targetSize[1]
        self.im_bounds = (left,right,top,bottom)
        #===================================#
        #== directory-related information ==#
        #===================================#
        self.X = images_in
        self.Y = images_out
        self.numImages = self.X.shape[0]
        outImgX = self.X[0]
        if len(outImgX)<3:
            outImgX = outImgX.reshape(outImgX.shape + (1,))
        self.image_size =outImgX.shape
        self.save_to_dir=save_to_dir
        self.save_format=save_format
    
    def __len__(self):
        return int(numpy.ceil(self.numImages/float(self.batch_size)))
    
    def __getitem__(self, idx):
        batchX = numpy.zeros((self.batch_size,self.image_size[0],self.image_size[1],self.image_size[2]),dtype=self.dtype)
        batchY = numpy.zeros((self.batch_size,self.image_size[0],self.image_size[1],self.image_size[2]),dtype=self.dtype)
        if self.useClip or self.useZoom:
            batchX = numpy.zeros((self.batch_size,self.targetSize[0],self.targetSize[1],self.image_size[2]),dtype=self.dtype)
            batchY = numpy.zeros((self.batch_size,self.targetSize[0],self.targetSize[1],self.image_size[2]),dtype=self.dtype)
        for j in itertools.islice(itertools.count(),0,self.batch_size):
            #imgIndex = min([(idx*self.batch_size)+j, self.numImages-1])
            imgIndex = ((idx*self.batch_size)+j) % (self.numImages-1)
            #if shuffle:
            #    batchIndex = numpy.random.randint(0, min([self.numImages,len(self.inputFileArray)]))
            """
            Load data from memory
            """
            outImgX = self.X[imgIndex]
            outImgY = self.Y[imgIndex]
            if len(outImgX)<3:
                outImgX = outImgX.reshape(outImgX.shape + (1,))
            if len(outImgY)<3:
                outImgY = outImgY.reshape(outImgY.shape + (1,))
            if outImgX.shape != outImgY.shape:
                raise RuntimeError("Input- and Output sizes do not match.")
            #self.image_size =outImgX.shape
            """
            Data augmentation
            """
            if self.useNormData:
                minValX = numpy.min(outImgX)
                maxValX = numpy.max(outImgX)
                outImgX = (outImgX-minValX)/(maxValX-minValX)
                outImgX = outImgX.astype(numpy.float32)
                minValY = numpy.min(outImgY)
                maxValY = numpy.max(outImgY)
                outImgY = (outImgY-minValY)/(maxValY-minValY)
                outImgY = outImgY.astype(numpy.float32)
            if self.useZoom:
                outImgX = ndimage.zoom(outImgX, [self.zoomFactor,self.zoomFactor,1], order=3)
                outImgY = ndimage.zoom(outImgY, [self.zoomFactor,self.zoomFactor,1], order=3)
            if self.useRotation:
                outImgX = ndimage.rotate(outImgX, numpy.random.uniform(self.rotationRange[0], self.rotationRange[1]), axes=(1,0), order=2, mode='mirror')
                outImgY = ndimage.rotate(outImgY, numpy.random.uniform(self.rotationRange[0], self.rotationRange[1]), axes=(1,0), order=2, mode='mirror')
            if self.useClip:
                outImgX = outImgX[self.im_bounds[0]:self.im_bounds[1],self.im_bounds[2]:self.im_bounds[3],0:self.targetSize[2]]
                outImgY = outImgY[self.im_bounds[0]:self.im_bounds[1],self.im_bounds[2]:self.im_bounds[3],0:self.targetSize[2]]
            if self.useAWGH: # only applies to input data
                outImgX = util.random_noise(outImgX, mode='gaussian', mean=self.gauss_mu, var=(self.gauss_stdDev*self.gauss_stdDev))
            if self.useFlipping:
                mode = numpy.random.randint(0,4)
                if mode == 0:   # no modification
                    pass
                if mode == 1:
                    outImgX = numpy.fliplr(outImgX)
                    outImgY = numpy.fliplr(outImgY)
                    #outImgX = ndimage.geometric_transform(outImgX, flipSpectralX, mode='nearest')
                    #outImgY = ndimage.geometric_transform(outImgY, flipSpectralX, mode='nearest')
                if mode == 2:
                    outImgX = numpy.flipud(outImgX)
                    outImgY = numpy.flipud(outImgY)
                    #outImgX = ndimage.geometric_transform(outImgX, flipSpectralY, mode='nearest')
                    #outImgY = ndimage.geometric_transform(outImgY, flipSpectralY, mode='nearest')
                if mode == 3:
                    outImgX = numpy.fliplr(outImgX)
                    outImgX = numpy.flipud(outImgX)
                    outImgY = numpy.fliplr(outImgY)
                    outImgY = numpy.flipud(outImgY)
                    #outImgX = ndimage.geometric_transform(outImgX, flipSpectralXY, mode='nearest')
                    #outImgY = ndimage.geometric_transform(outImgY, flipSpectralXY, mode='nearest')
            """
            Store data if requested
            """
            if self.save_to_dir != None:
                sXImg = array_to_img(outImgX[:,:,0])
                #save_img(os.path.join(self.save_to_dir,fname_in+"."+self.save_format),sXimg)
                sXimg.save(os.path.join(self.save_to_dir,fname_in+"."+self.save_format))
                sYImg = array_to_img(outImgY[:,:,0])
                #save_img(os.path.join(self.save_to_dir,fname_out+"."+self.save_format), sYImg)
                sYimg.save(os.path.join(self.save_to_dir,fname_out+"."+self.save_format))
            batchX[j] = outImgX
            batchY[j] = outImgY
        return batchX, batchY