#!/usr/bin/python
'''
Created on Sept 22, 2018

@author: christian
'''
import numpy
import h5py
from optparse import OptionParser
import itertools

if __name__ == '__main__':
    useFlatField = True
    optionParser = OptionParser("usage: %prog -i <input hdf5 file> [-n <field name inside mat file>]")
    optionParser.add_option("-i","--input",action="store",dest="input",default="",help="input path to hdf5 file")
    optionParser.add_option("-f", "--fieldName", action="store", dest="fieldName", default="data",
                            help="optional: field name to be extracted from .mat file")
    optionParser.add_option("-F", "--flatField", action="store", dest="flatFieldPath", help="path to flat field file")
    (options, args) = optionParser.parse_args()

    fieldName = options.fieldName
    dArray = numpy.zeros((1,1,1))

    f = h5py.File(options.input,'r')
    dArray = numpy.array(f[fieldName]['value'], order='F').transpose()
    f.close()

    dArray = numpy.squeeze(dArray)
    numDims = len(dArray.shape)
    arrayType = dArray.dtype

    if (options.flatFieldPath != None) and (len(options.flatFieldPath)>0) and useFlatField:
        f = h5py.File(options.flatFieldPath, "r")
        flatField = numpy.array(f['data']['value'], order='F').transpose()
        f.close()
        #dArray = numpy.clip(numpy.divide(dArray, flatField), 0.0, 1.0)
        for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[2]):
            minValX = 0
            maxValX = numpy.max(numpy.abs(dArray[:, :, channelIdx]))
            dArray[:, :, channelIdx] = (dArray[:, :, channelIdx] - minValX) / (maxValX - minValX)

    if numDims==2:
        mean = numpy.nanmean(dArray, None)
        median = numpy.nanmedian(dArray, None)
        stdDev = numpy.nanstd(dArray, None)
        variance = numpy.nanvar(dArray, None)
        print("Mean:\t\t\t{}".format(mean))
        print("Median:\t\t\t{}".format(median))
        print("std. Deviation:\t{}".format(stdDev))
        print("variance\t\t{}:".format(variance))
    elif numDims==3:
        meanArray = []
        medianArray = []
        stdDevArray = []
        varianceArray = []
        for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[2]):
            mean = numpy.nanmean(dArray[:,:,channelIdx], None)
            median = numpy.nanmedian(dArray[:,:,channelIdx], None)
            stdDev = numpy.nanstd(dArray[:,:,channelIdx], None)
            variance = numpy.nanvar(dArray[:,:,channelIdx], None)
            meanArray.append(mean)
            medianArray.append(median)
            stdDevArray.append(stdDev)
            varianceArray.append(variance)
            #print("=== Channel {} ===".format(channelIdx))
            #print("Mean:\t\t\t{}".format(mean))
            #print("Median:\t\t\t{}".format(median))
            #print("std. Deviation:\t{}".format(stdDev))
            #print("variance\t\t{}:".format(variance))
    elif numDims==4:
        meanArray = []
        medianArray = []
        stdDevArray = []
        varianceArray = []
        for channelIdx in itertools.islice(itertools.count(), 0, dArray.shape[3]):
            mean = numpy.nanmean(dArray[:,:,:,channelIdx], None)
            median = numpy.nanmedian(dArray[:,:,:,channelIdx], None)
            stdDev = numpy.nanstd(dArray[:,:,:,channelIdx], None)
            variance = numpy.nanvar(dArray[:,:,:,channelIdx], None)
            meanArray.append(mean)
            medianArray.append(median)
            stdDevArray.append(stdDev)
            varianceArray.append(variance)

    if numDims >= 3:
        print("Mean:\t\t\t{}".format(meanArray))
        print("Median:\t\t\t{}".format(medianArray))
        print("std. Deviation:\t{}".format(stdDevArray))
        print("variance:\t\t{}".format(varianceArray))


