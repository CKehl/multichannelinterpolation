#!python3
"""
Modified UNet training for scatter estimation 

Created on Oct 2, 2018

@author: christian (Christian Kehl)
@mail: chke@dtu.dk
"""

from __future__ import print_function

#from SpectralDimReduce import SpectralDimReduce
from SpectralDimUpscale import SpectralDimUpscale

#from SECTin_MECTout_sequencer import MECTsequencer
from SECTin_MECTout_phantom import MECTsequencer
import MemCache_CT

from argparse import ArgumentParser
import os
from scipy import io
import h5py
import pickle
import keras
import numpy
from callbacks import ModelCheckpoint
import threading
import multiprocessing
from multiprocessing.managers import BaseManager
import glob
import itertools
import re

"""
General parameters for learning
"""
input_indicator = "*Y*"
epochs = 100
keep_period = 1
input_image_size = (150,150)       # needs to fit with the actual data dim
target_image_size = (128,128)  # free to choose for resizing
input_channels = 1
output_channels = 32
#scaleFactor = min(input_image_size[0]/target_image_size[0], input_image_size[1]/target_image_size[1])
#SCALE_FACTOR = 1.706666667
epoch_steps=4096
batchSize=16
cache_reuse_period=192
#train_size = 4096
validation_size = 16


if __name__ == '__main__':
    description = ("simple auto-encoder for spectral up/downscaling.")
    option_parser = ArgumentParser(description=description)
    
    r_help = ("path to data folder with results (i.e. parent of output- and " +
              "model folder)")
    option_parser.add_argument("-r", "--resultpath",
                               action="store", dest="resultpath",
                               default="../data", help=r_help)
    m_help = ("name of the model file")
    option_parser.add_argument("-m", "--modelname",
                               action="store", dest="modelname",
                               default="reCT_epoch_{epoch:04d}", help=m_help)
    
    tdi_help = "training input data directory"
    option_parser.add_argument("-T", "--train-dir-in",
                               action="store", dest="train-dir-in",
                               default="../data/train", help=tdi_help)
    tdo_help = "training output data directory"
    option_parser.add_argument("-t", "--train-dir-out",
                               action="store", dest="train-dir-out",
                               default="../data/train", help=tdo_help)
    vdi_help = "validation input data directory"
    option_parser.add_argument("-V", "--valid-dir-in",
                               action="store", dest="valid-dir-in",
                               default="../data/train", help=vdi_help)
    vdo_help = "validation output data directory"
    option_parser.add_argument("-v", "--valid-dir-out",
                               action="store", dest="valid-dir-out",
                               default="../data/train", help=vdo_help)
    ff_help = "flat field file path"
    option_parser.add_argument("-F", "--flatfield-path",
                               action="store", dest="flatfield-path", help=ff_help)
    aug_help = ("augment input data")
    option_parser.add_argument("--augment-input", action="store_true",
                               dest="augment", help=aug_help)
    
    #c_help = ("name of the continuing, previous model")
    #option_parser.add_argument("-c", "--continue_model", action="store", dest="continue_mfile", default="", help=c_help)
    
    options = option_parser.parse_args()
    arg_dict = vars(options)
    
    training_directory_X = os.path.join(arg_dict["train-dir-in"])
    training_directory_Y = os.path.join(arg_dict["train-dir-out"])
    validation_directory_X = os.path.join(arg_dict["valid-dir-in"])
    validation_directory_Y = os.path.join(arg_dict["valid-dir-out"])
    resultpath = arg_dict["resultpath"]
    
    if not os.path.exists(os.path.join(resultpath, "output")):
        os.makedirs(os.path.join(resultpath, "output"))
    if not os.path.exists(os.path.join(resultpath, "models")):
        os.makedirs(os.path.join(resultpath, "models"))

    flatfield_path = None
    if(arg_dict["flatfield-path"]!=None):
        flatfield_path = arg_dict["flatfield-path"]
    
    modelfilename = arg_dict["modelname"]
    mhistfile = os.path.join(resultpath, "output", modelfilename + "_Thist.pkl")
    weightfile = os.path.join(resultpath, "models", modelfilename + "_weights.h5")
    modelfile = os.path.join(resultpath, "models", modelfilename + "_model.h5")

    latest_epoch, latest_model = 0, None
    ModelCheckpoint.remove_all_checkpoints(modelfile, weightfile)

    """
    Our UNet model is defined in the module unetwork stored locally. Using a
    factory class we can easyly generate a custom model using very few steps.
    
    unet_factory.dropout = None
    unet_factory.convolution_kernel_size = 3
    unet_factory.batch_normalization = False
    unet_factory.begin(image_shape=image_shape)
    unet_factory.generateLevels(init_filter_count=32, recursive_depth=4)
    model = unet_factory.finalize(final_filter_count=1)
    
    Here, using the specific network of Maier et al. 2018 on Deep Scatter Estimation (DSE)
    """
    #targetImageShape = batchY.shape + (1,)
    nnet = SpectralDimUpscale()
    nnet.begin((target_image_size[0],target_image_size[1],input_channels),(target_image_size[0],target_image_size[1],output_channels))
    model = nnet.buildNetwork()

    """
    Adding an optimizer (optional)---can be used to optimize gradient decent.
    We employ the rmsprop: divide the gradient by a running average of its
    recent magnitude.
    These are, for the moment, arbitrarily chosen by the original author. The
    work would benifit from a thorough walk through these choices.
    """
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    #opt = keras.optimizers.Adam(lr=0.0001, decay=1e-6, amsgrad=False)
    #opt = keras.optimizers.Adam(lr=0.0001, decay=1e-6, amsgrad=False)
    #opt = keras.optimizers.Adam(lr=0.0001, decay=0.00000075, amsgrad=True)
    #opt = keras.optimizers.Adam(lr=0.0001, decay=0.00000033, amsgrad=True)
    model.compile(loss='mae',  # standard cost function
                  optimizer=opt,
                  metrics=['mse'])  # Mean Absolute Error metrics

    """
    Data augmentation setup
    """    
    augment = arg_dict["augment"]

    #datagen = MECTsequencer_inMem(inDataTrain, inDataTrain, image_shape, batchSize, targetSize=(targetImageShape[0], targetImageShape[1]),
    #                              useResize=True,useZoom=False, useFlipping=True, flatField=flatfield)
    #validgen = MECTsequencer_inMem(inDataValid, inDataValid, image_shape, batchSize, targetSize=(targetImageShape[0], targetImageShape[1]),
    #                              useResize=True,useZoom=False, useFlipping=False, flatField=flatfield)
    trainingLock = multiprocessing.Lock()
    validationLock = multiprocessing.Lock()
    class Keras_Concurrency(BaseManager):
        pass


    manager = Keras_Concurrency()
    manager.register('MemoryCache', MemCache_CT.MemoryCache)
    manager.start()
    cache_mem = manager.MemoryCache()
    cache_mem.set_image_shape_x(input_image_size)
    cache_mem.set_number_channels_x(input_channels)
    cache_mem.set_image_shape_y(input_image_size)
    cache_mem.set_number_channels_y(output_channels)
    cache_mem.allocate()

    # for X-Ray: use flipping, median and AWGN
    datagen = MECTsequencer(batchSize,input_image_size,input_channels,target_image_size,output_channels, useResize=True,interim_size=(130,130),useCrop=True,useNormData=True,
                                       useShift=True, shift_range=(0,10), useRotation=True, rotation_range=(0,20), useZoom=True, zoom_factor_range=(0.9,1.1), useFlipping=True,
                                       useAWGN=False, useMedian=False, useGaussian=False, useCache=True)
    datagen.prepareDirectFileInput([training_directory_X], [training_directory_Y],flatfield_path)
    validgen = MECTsequencer(batchSize,input_image_size,input_channels,target_image_size,output_channels, useResize=True,interim_size=(130,130),useCrop=True,useNormData=True,
                                       useShift=False, useRotation=False, useZoom=False, useFlipping=False, useAWGN=False, useMedian=False, useGaussian=False, useCache=True)
    validgen.prepareDirectFileInput([validation_directory_X], [validation_directory_Y],flatfield_path)

    callbacks = []
    callbacks.append(ModelCheckpoint(modelpath=modelfile,
                                    weightspath=weightfile,
                                    period=1,
                                    auto_remove_model=True,
                                    auto_remove_weights=True,
                                    keep_period=keep_period))
    
    import subprocess
    subprocess.Popen("timeout 200 nvidia-smi --query-gpu=utilization.gpu,utilization.memory --format=csv -l 1 \ sed s/%//g > ./GPU_stats.log",shell=True)

    model_fitting_history = model.fit_generator(datagen,
                                                steps_per_epoch=int(epoch_steps / batchSize),
                                                validation_data=validgen,
                                                validation_steps = min([int(validgen.numImages/batchSize), int(128/batchSize)]),
                                                epochs=epochs,
                                                use_multiprocessing=True,
                                                workers=12,
                                                callbacks=callbacks)
    
    with open(mhistfile.format(epoch=epochs), 'wb') as file:
        pickle.dump(model_fitting_history.history, file)