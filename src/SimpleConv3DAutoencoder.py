from __future__ import print_function

from keras.models import Input, Model
from keras.layers import Dense, Conv3D, Concatenate, MaxPooling3D, Add
from keras.layers import UpSampling3D, Dropout, BatchNormalization

class SimpleAutoencoder(object):
    """A simple multi-channel autoencoder in Keras"""

    def __init__(self):
        super(SimpleAutoencoder, self).__init__()
        self._activation = 'relu'
        self._dropout = True

        self.dataShape = None
        self.input = None  # Holder for Model input
        self.output = None  # Holder for Model output

    def begin(self, image_shape):
        """
        Input here is an image of the forward scatter intensity
        of the measured data (c_0=1); In imaging terms, that's a 1-channel 2D image.
        """
        self.dataShape = image_shape
        print("input shape {}".format(image_shape))
        self.input = Input(shape=image_shape)

    def finalize(self):
        """
        The expected output data is the scatter prediction; in train-test scenarios,
        this is the Monte Carlo simulation of the scattering.
        The output variable holds the network to achieve that.
        The function returns the model itself.
        """
        if self.input is None:
            raise RuntimeError("Missing an input. Use begin().")
        if self.output is None:
            raise RuntimeError("Missing an output. Use buildNetwork().")
        return Model(inputs=self.input, outputs=self.output)

    def buildNetwork(self, inShapePerImage=None):
        """
        This function builds the network exactly as-is from the paper of Maier et al.
        """
        #=== Downsizing Convolutional ===#
        #print("Created input layer with shape {}".format(self.input.shape))
        #numChannels = self.dataShape[2]
        ## Encoder
        #conv1_1_1 = Conv2D(int(numChannels / 2), (3, 3), activation=self._activation, padding='same', input_shape=inShapePerImage, data_format="channels_last")(self.input)
        #pool1 = MaxPooling2D((2, 2), padding='same')(conv1_1_1)
        ##pool1 = Dense(int(numChannels / 4), activation=self._activation)(conv1_1_2)
        #print("Created conv layer 1 with shape {}".format(pool1.shape))
        #conv1_2_1 = Conv2D(int(numChannels / 8), (3, 3), activation=self._activation, padding='same')(pool1)
        #pool2 = MaxPooling2D((2, 2), padding='same')(conv1_2_1)
        ##pool2 = Dense(int(numChannels / 16), activation=self._activation)(conv1_2_2)
        #print("Created conv layer 2 with shape {}".format(pool2.shape))
        #conv1_3_1 = Conv2D(int(numChannels / 16), (3, 3), activation=self._activation, padding='same')(pool2)
        #pool3 = MaxPooling2D((2, 2), padding='same')(conv1_3_1)
        ##pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        #print("Created conv layer 3 with shape {}".format(pool3.shape))
        #convcenter = Conv2D(1, (3, 3), activation=self._activation, padding='same')(pool3)
        ## Decoder
        #conv2_1_1 = Conv2D(int(numChannels / 16), (3, 3), activation=self._activation, padding='same')(convcenter)
        #up1 = UpSampling2D((2, 2))(conv2_1_1)
        ##up1 = Dense(int(numChannels / 16), activation=self._activation)(conv2_1_2)
        #print("Created deconv layer 3 with shape {}".format(up1.shape))
        #conv2_2_1 = Conv2D(int(numChannels / 8), (3, 3), activation=self._activation, padding='same')(up1)
        #up2 = UpSampling2D((2, 2))(conv2_2_1)
        ##up2 = Dense(int(numChannels / 4), activation=self._activation)(conv2_2_2)
        #print("Created deconv layer 2 with shape {}".format(up2.shape))
        #conv2_3_1 = Conv2D(int(numChannels / 2), (3, 3), activation=self._activation, padding='same')(up2)
        #up3 = UpSampling2D((2, 2))(conv2_3_1)
        ##up3 = Dense(int(numChannels / 2), activation=self._activation)(conv2_3_2)
        #print("Created deconv layer 1 with shape {}".format(up3.shape))
        #self.output = Conv2D(numChannels, (3, 3), activation='sigmoid', padding='same', data_format="channels_last")(up3)
        #print("Created output layer with shape {}".format(self.output.shape))


        print("Created input layer with shape {}".format(self.input.shape))
        numChannels = self.dataShape[2]
        # Encoder
        conv1_1_1 = Conv3D(2, (3, 3, 3), activation=self._activation, padding='same')(self.input)
        pool1 = MaxPooling3D((2, 2, 2), padding='same')(conv1_1_1) #256x256 -> 128x128
        # pool1 = Dense(int(numChannels / 4), activation=self._activation)(conv1_1_2)
        print("Created conv layer 1 with shape {}".format(pool1.shape))
        conv1_2_1 = Conv3D(4, (3, 3, 3), activation=self._activation, padding='same')(pool1)
        pool2 = MaxPooling3D((2, 2, 2), padding='same')(conv1_2_1) #128x128 -> 64x64
        # pool2 = Dense(int(numChannels / 16), activation=self._activation)(conv1_2_2)
        print("Created conv layer 2 with shape {}".format(pool2.shape))
        conv1_3_1 = Conv3D(8, (3, 3, 3), activation=self._activation, padding='same')(pool2)
        pool3 = MaxPooling3D((2, 2, 2), padding='same')(conv1_3_1) #64x64 -> 32x32
        # pool3 = Dense(int(numChannels / 8), activation=self._activation)(conv1_3_2)
        print("Created conv layer 3 with shape {}".format(pool3.shape))

        convcenter = Conv3D(16, (3, 3, 3), activation=self._activation, padding='same')(pool3)

        # Decoder
        conv2_3_1 = Conv3D(8, (3, 3, 3), activation=self._activation, padding='same')(convcenter)
        up1 = UpSampling3D((2, 2, 2))(conv2_3_1)
        # up1 = Dense(int(numChannels / 16), activation=self._activation)(conv2_1_2)
        print("Created deconv layer 3 with shape {}".format(up1.shape))
        conv2_4_1 = Conv3D(4, (3, 3, 3), activation=self._activation, padding='same')(up1)
        up2 = UpSampling3D((2, 2, 2))(conv2_4_1)
        # up2 = Dense(int(numChannels / 4), activation=self._activation)(conv2_2_2)
        print("Created deconv layer 2 with shape {}".format(up2.shape))
        conv2_5_1 = Conv3D(2, (3, 3, 3), activation=self._activation, padding='same')(up2)
        up3 = UpSampling3D((2, 2, 2))(conv2_5_1)
        # up3 = Dense(int(numChannels / 2), activation=self._activation)(conv2_3_2)
        print("Created deconv layer 1 with shape {}".format(up3.shape))
        self.output = Conv3D(1, (3, 3, 3), activation='sigmoid', padding='same')(up3)
        print("Created output layer with shape {}".format(self.output.shape))